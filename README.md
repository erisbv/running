### Developer
* NAME: _ERIS BATISTA_
* E-MAIL: [_erisbv@gmail.com_](mailto:erisbv@gmail.com)
* Linked-ID: [Eris Batista Profile](https://www.linkedin.com/in/eris-batista) 

### Steps
~~~
- Clone package:
    - $ git clone https://erisbv@bitbucket.org/erisbv/running.git

- Json Insomnia em root path
    - _insomnia.config.json

- Information Login:
    - All users with password: 123
    - Access Token Login Generate

- Information DB:
    - DB_CONNECTION=mysql
    - DB_HOST=mysql
    - DB_PORT=3306
    - DB_DATABASE=running
    - DB_USERNAME=root
    - DB_PASSWORD=root

- Information LocalHost:
    - The domain in sites nginx (path docker: Laradock):
        - running.local
        - root: src/public <=> /var/www/public

- Install Commands:
    - $ cd {project_path}/.docker
    - $ docker-compose up -d --build mysql nginx
    - $ docker-compose exec workspace bash
    - $ npm install -g npm@7.7.5 (optional)
    - $ composer install (".env was not ignored", it is part of the root)
    - $ php artisan migrate:fresh --seed
~~~

## Routes
```
+-----------+-------------------------------------+---------------------------+-------------------------------------------------------------------------+
| Method    | URI                                 | Name                      | Action                                                                  |
+-----------+-------------------------------------+---------------------------+-------------------------------------------------------------------------+
| POST      | api/login                           | auth.login                | App\Http\Controllers\Auth\LoginController@store                         |
| POST      | api/register                        | auth.register             | App\Http\Controllers\Auth\RegisterController@store                      |
| GET|HEAD  | api/list/sort-age                   | list-sort-age             | App\Http\Controllers\API\v1\RunningController@listRunningByBirthday     |
| GET|HEAD  | api/list/sort-ranking               | list-sort-age             | App\Http\Controllers\API\v1\RunningController@listRunningRanking        |
| POST      | api/logout                          | auth.logout               | App\Http\Controllers\Auth\LoginController@destroy                       |
| GET|HEAD  | api/runnings                        | runnings.index            | App\Http\Controllers\API\v1\RunningController@index                     |
| POST      | api/runnings                        | runnings.store            | App\Http\Controllers\API\v1\RunningController@store                     |
| POST      | api/runnings/add-result             | user.finished-running     | App\Http\Controllers\API\v1\RunningController@finishedRunning           |
| DELETE    | api/runnings/{running}              | runnings.destroy          | App\Http\Controllers\API\v1\RunningController@destroy                   |
| PUT|PATCH | api/runnings/{running}              | runnings.update           | App\Http\Controllers\API\v1\RunningController@update                    |
| GET|HEAD  | api/runnings/{running}              | runnings.show             | App\Http\Controllers\API\v1\RunningController@show                      |
| GET|HEAD  | api/users                           | users.index               | App\Http\Controllers\API\v1\UserController@index                        |
| POST      | api/users/add-running               | user.registration-running | App\Http\Controllers\API\v1\UserController@registrationRunning          |
| DELETE    | api/users/{user}                    | users.destroy             | App\Http\Controllers\API\v1\UserController@destroy                      |
| PUT|PATCH | api/users/{user}                    | users.update              | App\Http\Controllers\API\v1\UserController@update                       |
| GET|HEAD  | api/users/{user}                    | users.show                | App\Http\Controllers\API\v1\UserController@show                         |
+-----------+-------------------------------------+---------------------------+-------------------------------------------------------------------------+
```

## Comentários
<p>
Não foi criado um modelo pra a Tabela Pivô, sendo assim, o tempo de milisegundos está sendo calculado no Carbon.
</p>

<hr />
<p>
As idades foram passadas com limites iguais, sendo assim, cada grupo finaliza um a menos que o próximo grupo.
</p>

```
$ranges = [
    '18-24' => 24,
    '25-34' => 34,
    '35-44' => 44,
    '45-54' => 54,
    '55+' => 55,
];
```

