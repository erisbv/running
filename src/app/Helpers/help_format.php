<?php
use Illuminate\Routing\Route;
use Illuminate\Support\Str;

/**
 * @param string $numbers
 * @return string
 */
function formatNumMobile(string $numbers): string
{
    $number = getNumbers($numbers);
    $format = '(\d\d)(\d\d\d\d\d)(\d\d\d\d)';
    $replace = "($1) $2-$3";

    if ((strlen($number) === 9)) {
        $format = '(\d\d\d\d\d)(\d\d\d\d)';
        $replace = "$1-$2";
    }

    $number_format = preg_replace("/$format/", $replace, $number);
    return preg_replace('/\s+/', ' ', $number_format);
}

/**
 * @param string|null $val
 * @param bool $symbol
 * @return string
 */
function formatNumToReal(string $val = null, bool $symbol = true): string
{
    if (empty($val)) {
        return ($symbol) ? 'R$ 0,00' : '0,0';
    }

    $val_float = strpos($val, ',') !== false ? formatNumToFloat($val) : $val;

    if($symbol) {
        return 'R$ ' . number_format($val_float, 2, ',', '.');
    }
    return number_format($val_float, 2, ',', '.');
}

/**
 * @param string|float|null $val
 * @return float
 */
function formatNumToFloat($val = null): float
{
    if (empty($val)) {
        return 0;
    }
    if(is_float($val) || strpos($val, ',') === false){
        return $val;
    }
    $val = str_replace(['R$', '$', '.', ','], ['','','', '.'], $val);
    return (float)$val;
}

/**
 * @param string $text
 * @param string $type
 * @return string
 * @throws Exception
 */
function formatLabel(string $text, string $type = 'dark'): string
{
    $types = ['danger', 'primary', 'success', 'warning', 'info', 'default', 'dark', 'secondary'];
    if (!in_array($type, $types, true)) {
        throw new RuntimeException('Estilo de label inválido.');
    }
    return preg_replace(['/_TP_/', '/_TXT_/'], ['badge-' . $type, $text], '<span class="badge _TP_">_TXT_</span>');

}

/**
 * @param string $date
 * @return string|null
 */
function formatDateBr(string $date): ?string
{
    return date('d/m/Y', strtotime($date));
}

/**
 * @param string $date
 * @return string|null
 */
function formatDateIntl(string $date): ?string
{
    return date('Y-m-d', strtotime($date));
}

/**
 * @param string $value
 * @return string
 */
function formatCnpjCpf(string $value): string
{
    // CLEAR STRING
    $cnpj_cpf = getNumbers($value);

    if (mb_strlen($cnpj_cpf) === 11) {
        return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cnpj_cpf);
    }

    return preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
}

/**
 * @param $string
 * @param string $separator
 * @param bool $lower_first_character
 * @return string
 */
function formatToCamelCase($string, $separator = '-', $lower_first_character = false): string
{

    $str = Str::title(str_replace($separator, ' ', $string));

    if ($lower_first_character) {
        $str = lcfirst($str);
    }

    return $str;
}

/**
 * @param string $value
 * @return string
 */
function formatCep(string $value): string
{
    $numbers = getNumbers($value);
    return preg_replace("/(\d{2})(\d{3})(\d{3})/", "\$1.\$2-\$3", $numbers);
}
