<?php

use Illuminate\Routing\Route;
use Illuminate\Support\Str;

/**
 * method isCpf
 * <p>Verifica se o número enviado ta dentro do valor correto de um CPF</p>
 *
 * @param string $value
 * @param bool $return_formatted
 * @return bool|string
 */
function isCpf(string $value, bool $return_formatted = true)
{
    $cpf = getNumbers($value);
    if (strlen($cpf) !== 11 || preg_match('/(\d)\1{10}/', $cpf)) {
        return false;
    }

    // Faz o calculo para validar o CPF
    for ($t = 9; $t < 11; $t++) {
        for ($d = 0, $c = 0; $c < $t; $c++) {
            $d += $cpf[$c] * (($t + 1) - $c);
        }
        $d = ((10 * $d) % 11) % 10;
        if ((int)$cpf[$c] !== $d) {
            return false;
        }
    }

    if (!$return_formatted) {
        return true;
    }
    return formatCnpjCpf($value);
}

/**
 * method isCnpj
 * <p>Verifica se o número enviado ta dentro do valor correto de um CNPJ</p>
 *
 * @param string $value
 * @param bool $return_format
 * @return bool|string
 */
function isCnpj(string $value, bool $return_format = true)
{
    if(empty($value)){
        return false;
    }
    $numbers = getNumbers($value);
    if (strlen($numbers) !== 14 || preg_match('/(\d)\1{13}/', $numbers)) {
        return false;
    }

    // Valida primeiro dígito verificador
    for ($i = 0, $j = 5, $total = 0; $i < 12; $i++) {
        $total += $numbers[$i] * $j;
        $j = ($j === 2) ? 9 : $j - 1;
    }

    $total_diff = $total % 11;
    if ((int)$numbers[12] !== ($total_diff < 2 ? 0 : 11 - $total_diff)) {
        return false;
    }

    // Valida segundo dígito verificador
    for ($i = 0, $j = 6, $total_digit = 0; $i < 13; $i++) {
        $total_digit += $numbers[$i] * $j;
        $j = ($j === 2) ? 9 : $j - 1;
    }

    $total_digit_diff = $total_digit % 11;
    if ((int)$numbers[13] !== ($total_digit_diff < 2 ? 0 : 11 - $total_digit_diff)) {
        return false;
    }
    return ($return_format) ? formatCnpjCpf($value) : true;
}

/**
 * method isCpfCnpj
 * <p>Verifica se o número enviado ta dentro do valor correto de um CPF ou CNPJ</p>
 *
 * @param string $value
 * @param bool $return_format
 * @return bool|string
 */
function isCpfCnpj(string $value, bool $return_format = true)
{
    $numbers = getNumbers($value);
    if (mb_strlen($numbers) === 11) {
        return isCpf($value, $return_format);
    }
    return isCnpj($value, $return_format);
}

/**
 * @param string $number
 * @return bool
 */
function isMobile(string $number): bool
{
    return preg_match('/^\(\d{2}\)\s?\d{4,5}-\d{4}$/', $number) > 0;
}

/**
 * Valida o formato do celular junto com o ddd
 *
 * @param $value
 * @return bool
 */
function isMobileDdd($value): bool
{
    $mobile = formatNumMobile($value);
    return preg_match('/^\(\d{2}\)\s?\d{4,5}-\d{4}$/', $mobile) > 0;
}

/**
 * Valida o formato do telefone junto com o ddd
 * @param string $value
 * @return boolean
 */
function isPhoneDdd($value): bool
{
    return preg_match('/^\(\d{2}\)\s?\d{4}-\d{4}$/', $value) > 0;
}

/**
 * Valida o formato do telefone
 * @param string $attribute
 * @param string $value
 * @return boolean
 */
function isPhone($value): bool
{
    return preg_match('/^\d{4}-\d{4}$/', $value) > 0;
}

/**
 * Valida o formato do cpf
 * @param string $attribute
 * @param string $value
 * @return boolean
 */
function isFormatCpf($value): bool
{
    return preg_match('/^\d{3}\.\d{3}\.\d{3}-\d{2}$/', $value) > 0;
}

/**
 * Valida o formato do cnpj
 * @param string $attribute
 * @param string $value
 * @return boolean
 */
function isFormatCnpj($value): bool
{
    return preg_match('/^\d{2}\.\d{3}\.\d{3}\/\d{4}-\d{2}$/', $value) > 0;
}

/**
 * Valida se o CNH é válido
 * @param string $value
 * @return boolean
 */
function isCnh($value): bool
{
    $ret = false;

    if ((strlen($input = preg_replace('/[\D]/', '', $value)) === 11)
        && (str_repeat($input[1], 11) !== $input)) {
        $dsc = 0;

        for ($i = 0, $j = 9, $v = 0; $i < 9; ++$i, --$j) {
            $v += (int)$input[$i] * $j;
        }

        if (($vl1 = $v % 11) >= 10) {
            $vl1 = 0;
            $dsc = 2;
        }

        for ($i = 0, $j = 1, $v = 0; $i < 9; ++$i, ++$j) {
            $v += (int)$input[$i] * $j;
        }

        $vl2 = ($x = ($v % 11)) >= 10 ? 0 : $x - $dsc;

        $ret = sprintf('%d%d', $vl1, $vl2) === substr($input, -2);
    }

    return $ret;
}

/**
 * Valida se o data está no formato 31/12/1969
 * @param string $value
 * @return boolean
 */
function isDateBr($value): bool
{
    $regex = '/^(0[1-9]|[1-2][0-9]|3[01])\/(0[1-9]|1[0-2])\/\d{4}$/';

    return preg_match($regex, $value) > 0;
}

/**
 * Valida se o formato de CEP está correto
 *
 * @param string $value
 * @return boolean
 */
function isFormatCep($value): bool
{
    return preg_match('/^\d{2}\.?\d{3}-\d{3}$/', $value) > 0;
}



