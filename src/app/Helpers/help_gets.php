<?php

use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Route;
use Illuminate\Support\Str;

if (!function_exists('getResponseJson')) {
    /**
     * @param string $msg
     * @param array $data
     * @param bool $resource
     * @return JsonResponse|array
     */
    function getResponseJson(string $msg, array $data, bool $resource = false)
    {
        $data = ['message' => $msg, 'data' => $data,];
        return !$resource? response()->json($data,Response::HTTP_OK) : $data;
    }
}

if (!function_exists('getRequestRoute')) {
    /**
     * @return Route|object|string|null
     */
    function getRequestRoute()
    {
        return request()->route();
    }
}

if (!function_exists('getUrl')) {
    /**
     * @param $uri
     * @param $end_point
     * @return string
     */
    function getUrl($uri, $end_point): string
    {
        return str_replace(['///', '////'], '//', $uri . '/' . $end_point);
    }
}

if (!function_exists('getNumbers')) {
    /**
     * @param string $text
     * @return string|null
     */
    function getNumbers(string $text): ?string
    {
        if (empty($text)) {
            throw new RuntimeException("Validação numérica interrompida, parâmetro nulo.");
        }
        return preg_replace('/\D/', '', $text);
    }
}
