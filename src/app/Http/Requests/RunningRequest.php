<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RunningRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'run_type_id' => 'required|numeric',
            'name' => 'required|string|max:50|unique:runnings',
            'start_at' => 'required|date',
            'finished_at' => 'nullable|date|after:start_at',
        ];
    }

    /**
     * @return array
     */
    public function attributes()
    {
        return [
            'run_type_id' => 'Tipo de Corrida',
            'name' => 'Nome da Corrida',
            'start_at' => 'Data e Hora da Largada',
            'finished_at' => 'Data e Hora da Chegada',
        ];
    }

    /**
     * @return array
     */
    public function messages()
    {
        return [
            'name.unique' => 'Não é possível cadastrar duas corridas com o mesmo nome',
            'finished_at.after_or_equal' => 'A data/hora de chegada não pode ser inferior a data/hora da largada'
        ];
    }
}
