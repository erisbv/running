<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\Services\ServiceUser;
use Carbon\Carbon;
use http\Exception\InvalidArgumentException;
use http\Exception\RuntimeException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;
use Illuminate\View\View;

/**
 * Class RegisterController
 * @package App\Http\Controllers\Auth
 *
 * @property-read ServiceUser $ServiceUser
 */
class RegisterController extends Controller
{

    use RegistersUsers;
    /**
     * @var ServiceUser $ServiceUser
     */
    private ServiceUser $ServiceUser;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * RegisterController constructor.
     * @param ServiceUser $serviceUser
     */
    public function __construct(ServiceUser $serviceUser)
    {
        $this->middleware('guest');
        $this->ServiceUser = $serviceUser;
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        Validator::extend('olderThan', function ($attribute, $value, $parameters) {
            $minAge = (!empty($parameters)) ? (int)$parameters[0] : 18;
            return Carbon::now()->diff(new Carbon($value))->y >= $minAge;
        });

        return Validator::make($data, [
            'birthday' => 'required|date|olderThan',
            'cpf' => 'required|string|min:11|max:14',
            'email' => 'required|string|email|max:100|unique:users',
            'name' => 'required|string|max:100',
            'password' => 'required|string|min:6|max:8|confirmed',
        ]);
    }

    /**
     * Display the registration view.
     *
     * @return View
     */
    public function create()
    {
        return view('auth.register');
    }

    /**
     * @param Request $request
     * @return ResponseFactory|Response
     * @throws ValidationException
     * @throws \Throwable|RuntimeException|ValidationException
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token']);
        $validator = $this->validator($data);

        if ($validator->fails()) {
            throw new ValidationException($validator->getMessageBag(),Response::HTTP_UNAUTHORIZED);
        }

        if ($this->ServiceUser->existsCPF($request->get('cpf'))) {
            throw new \RuntimeException('CPF já registrado no sistema.', Response::HTTP_UNAUTHORIZED);
        }

        $user = $this->ServiceUser->store($data);
        Auth::login($user);

        return getResponseJson(__('register.success'), $user->toArray());
    }
}
