<?php

namespace App\Http\Controllers\Auth;

use App\Models\User;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Providers\RouteServiceProvider;
use App\Services\ServiceUser;
use http\Exception\InvalidArgumentException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Validation\UnauthorizedException;
use phpDocumentor\Reflection\DocBlock\Tags\Throws;
use Illuminate\Http\{JsonResponse, RedirectResponse, Request, Response};
use Illuminate\Support\Facades\{Auth, Hash, Validator};
use Illuminate\Validation\ValidationException;

/**
 * Class LoginController
 * @package App\Http\Controllers\Auth
 *
 * @property-read ServiceUser $UserService
 *
 */
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected string $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * @param LoginRequest $request
     * @return ResponseFactory|Response
     * @throws AuthenticationException
     */
    public function store(Request $request)
    {
        $user = ServiceUser::make()->getUserByEmail($request->get('email'));
        if (!$user || !Hash::check($request->get('password'), $user->password)) {
            throw new UnauthorizedException("Usuário inválido.", Response::HTTP_UNAUTHORIZED);
        }
        $user->setAttribute('token', $user->createToken('app-token')->plainTextToken);
        return getResponseJson('Login efetuado com sucesso (JWT active).', $user->toArray());
    }

    /**
     * Destroy an authenticated session.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }

    /**
     * @param Request $request
     * @return array|JsonResponse
     */
    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();
        return getResponseJson('Logout efetuado! Token(s) revogado(s).');
    }
}
