<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Services\ServiceUser;
use Carbon\Carbon;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

/**
 * Class UserController
 * @package App\Http\Controllers\API\v1
 *
 * @property-read User|User[] $User
 * @property-read ServiceUser $ServiceUser
 *
 */
class UserController extends Controller
{
    /**
     * @var ServiceUser $ServiceUser
     */
    private ServiceUser $ServiceUser;

    /**
     * UserController constructor.
     * @param ServiceUser $serviceUser
     */
    public function __construct(ServiceUser $serviceUser)
    {
        $this->ServiceUser = $serviceUser;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return response()->json($this->ServiceUser->pagination($request));
    }

    /**
     * Display the specified resource.
     *
     * @param string $user_id
     * @return Response
     */
    public function show(string $user_id)
    {
        $user = $this->ServiceUser->getUser($user_id);
        return response()->json($user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function registrationRunning(Request $request)
    {
        $user_id = $request->get('user_id', null);
        $running_id = $request->get('running_id', null);
        if (empty($user_id) || empty($running_id)) {
            throw new \HttpInvalidParamException("Parâmetros inválidos.");
        }

        $data = $this->ServiceUser->registrationRunning($user_id, $running_id);
        return getResponseJson('Registro efetuado com sucesso!', $data->toArray());
    }
}
