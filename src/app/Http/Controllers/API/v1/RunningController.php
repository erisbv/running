<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\RunningRequest;
use App\Http\Resources\RunningCollection;
use App\Http\Resources\RunningGroupCollection;
use App\Models\User;
use App\Services\ServiceRunning;
use App\Services\ServiceUser;
use Carbon\Carbon;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

/**
 * Class RunningController
 * @package App\Http\Controllers\API\v1
 *
 * @property-read User|User[] $User
 * @property-read ServiceUser $ServiceUser
 * @property-read ServiceRunning $ServiceRunning
 *
 */
class RunningController extends Controller
{
    /**
     * @var ServiceUser $ServiceUser
     */
    private ServiceUser $ServiceUser;

    /**
     * @var ServiceRunning $ServiceRunning
     */
    private ServiceRunning $ServiceRunning;

    /**
     * UserController constructor.
     * @param ServiceUser $serviceUser
     * @param ServiceRunning $serviceRunning
     */
    public function __construct(ServiceUser $serviceUser, ServiceRunning $serviceRunning)
    {
        $this->ServiceUser = $serviceUser;
        $this->ServiceRunning = $serviceRunning;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        return response()->json($this->ServiceRunning->pagination($request));
    }

    /**
     * Display the specified resource.
     *
     * @param string $running_id
     * @return Response
     */
    public function show(string $running_id)
    {
        $user = $this->ServiceRunning->getRunning($running_id);
        return response()->json($user);
    }

    /**
     * Register resource in storage.
     * @param RunningRequest $request
     * @return JsonResponse
     * @throws \Throwable
     */
    public function store(RunningRequest $request)
    {
        $Running = $this->ServiceRunning->store($request->toArray());
        return getResponseJson('Race registered successfully!', $Running->toArray());
    }


    public function finishedRunning(Request $request)
    {

        $user_id = $request->get('user_id', null);
        $running_id = $request->get('running_id', null);
        if (empty($user_id) || empty($running_id)) {
            throw new \HttpInvalidParamException("Parâmetros inválidos.");
        }

        $data = $this->ServiceRunning->finishedRunning($user_id, $running_id);
        return getResponseJson('Registro efetuado com sucesso!', $data->toArray());
    }

    /**
     * @return RunningCollection
     */
    public function listRunningByBirthday()
    {
        $data = $this->ServiceRunning->listRunningByBirthday();
        return new RunningGroupCollection($data);
    }

    /**
     * @return RunningCollection
     */
    public function listRunningRanking()
    {
        $data = $this->ServiceRunning->listRunningRanking();
        return new RunningCollection($data);
    }
}
