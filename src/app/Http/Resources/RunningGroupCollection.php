<?php

namespace App\Http\Resources;

use App\Models\Running;
use App\Models\User;
use App\Services\ServiceRunning;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Carbon;

/**
 * Class RunningGroupCollection
 * @package App\Http\Resources
 */
class RunningGroupCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [];
        foreach ($this->getIterator() as $key => $_data) {
            /**
             * @var Running $Running
             */
            $Running = ServiceRunning::make()->getModel();
            $Running->setRawAttributes($_data['running'])->setRelation('run_type', $_data['run_type']);

            $data[$key]['running']['id'] = $Running->id;
            $data[$key]['running']['name'] = $Running->name;
            $data[$key]['running']['type'] = $Running->runType->run_type_name;
            $data[$key]['running']['start'] = Carbon::parse($Running->start_at)->toDateTimeString('milliseconds');
            $data[$key]['running']['arrival'] = Carbon::parse($Running->finished_at)->toTimeString('milliseconds');
            $data[$key]['running']['max'] = [
                'users' => $Running->total_users,
                'time' => $Running->total_time
            ];
            foreach ($_data['users'] as $user_group => $users) {
                /**
                 * @var User[] $users
                 * @var User $user
                 */
                foreach ($users as $index => $user) {
                    $data[$key]['running']['users'][$user_group][] = [
                        'name' => $user->name,
                        'age' => (int)Carbon::parse($user->birthday)->age,
                        'raking_global' => $user->pivot->ranking,
                        'raking_age' => $index+1,
                        'time' => Carbon::parse($user->pivot->finished_at)->toTimeString('milliseconds'),
                    ];
                }
            }
        }
        return getResponseJson('Lista de Corridas pela Data de Nascimento e Classificação', $data, true);
    }
}
