<?php

namespace App\Http\Resources;

use App\Models\Running;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * Class RunningCollection
 * @package App\Http\Resources
 */
class RunningCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [];
        foreach ($this->getIterator() as $key => $running) {
            /**
             * @var Running $running
             */
            $data[$key]['running']['id'] = $running->id;
            $data[$key]['running']['name'] = $running->name;
            $data[$key]['running']['type'] = $running->runType->run_type_name;
            $data[$key]['running']['start'] = Carbon::parse($running->start_at)->toDateTimeString('milliseconds');
            $data[$key]['running']['arrival'] = Carbon::parse($running->finished_at)->toTimeString('milliseconds');
            $data[$key]['running']['max'] = [
                'users' => $running->total_users,
                'time' => $running->total_time
            ];
            foreach ($running->users as $key_inner => $user) {
                $data[$key]['running']['users'][] = [
                    'name' => $user->name,
                    'age' => (int)Carbon::parse($user->birthday)->age,
                    'raking' => $user->pivot->ranking,
                    'time' => Carbon::parse($user->pivot->finished_at)->toTimeString('milliseconds'),
                    'age_group' => $user->age_group
                ];
            }
        }
        return getResponseJson('Lista de Corridas pela Data de Nascimento e Classificação', $data, true);
    }

}
