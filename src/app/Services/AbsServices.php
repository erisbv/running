<?php


namespace App\Services;

/**
 * Class AbsServices
 * @package App\Services
 */
class AbsServices
{
    protected $model;

    /**
     * @return static
     */
    public static function make()
    {
        $class_basename = static::class;
        $class_basename = class_basename($class_basename);
        $class = str_replace('Service', '', $class_basename);
        $class = "\App\Models\\" . $class;
        if(!class_exists($class)){
            throw new \RuntimeException("Classe invocada não existe: {$class}::{$class_basename}");
        }
        return new static(new $class);
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model::getModel();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->model::find($id);
    }

    /**
     * @param array $criteria
     * @return Collection[]|Collection|Model
     */
    public function findBy(array $criteria)
    {
        return $this->model::where($criteria)->get();
    }

    /**
     * @param $id
     * @return EloquentBuilder|EloquentBuilder[]|Collection|Model|Model[]
     */
    public function findOrFail($id)
    {
        return $this->model::findOrFail($id);
    }

    /**
     * @return Collection|Model[]
     */
    public function findAll()
    {
        return $this->model::all();
    }

    /**
     * @param array $criteria
     * @return mixed
     */
    public function findByFirst(array $criteria)
    {
        return $this->model::where($criteria)->first();
    }

    /**
     * @param array $fields_value
     * @return Collection[]|Model[]
     */
    public function findIn(array $fields_value): array
    {
        return $this->model::whereIn('id', $fields_value)->get();
    }

    /**
     * @param array $data
     * @return mixed
     */
    public function firstOrCreate(array $data)
    {
        return $this->model::firstOrCreate($data);
    }

    /**
     * @param $id
     */
    public function setInstanceById($id)
    {
        $result = $this->findOrFail($id);
        $this->model = clone $result;
    }

    /**
     * @param array $where
     * @return bool
     */
    public function exists(array $where): bool
    {
        $std_class = get_class($this->model);
        /**
         * @var EloquentBuilder $model
         */
        $model = new $std_class();
        return $model::where($where)->exists();
    }

    /**
     * @param string $entity_id
     * @return EloquentBuilder|Model
     */
    public function filterByEntityId(string $entity_id)
    {
        return $this->model::where('entity_id', $entity_id);
    }

    /**
     * @param mixed ...$columns
     * @return EloquentBuilder
     */
    public function where(...$columns): EloquentBuilder
    {
        return $this->model::where($columns);
    }

    /**
     * @param mixed ...$relations
     * @return EloquentBuilder|$this
     */
    public function with(...$relations)
    {
        return $this->model::with($relations);
    }

    public function create(array $data)
    {
        return $this->model::create($data);
    }

}
