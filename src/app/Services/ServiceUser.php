<?php


namespace App\Services;


use App\Models\Running;
use App\Models\User;
use http\Exception\InvalidArgumentException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Class ServiceUser
 * @package App\Services
 *
 * @property-read array $_rules
 * @property-read Model|User|Collection|User[] $model
 *
 */
class ServiceUser extends AbsServices
{

    /**
     * @var array $_rules
     */
    private array $_rules = ['email' => 'required|string|email|max:255', 'password' => 'required|min:8'];

    /**
     * UserService constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }

    public function getUser(string $user_id)
    {
        return $this->findOrFail($user_id)->load('Runnings');
    }

    /**
     * @param string $email
     * @return User|null
     * @throws InvalidArgumentException
     */
    public function getUserByEmail(string $email)
    {
        return $this->model->where('email', $email)->first();
    }

    /**
     * @param array $data_user
     * @return User|Model
     * @throws AuthenticationException
     */
    public function store(array $data_user)
    {
        $user = $this->model->setRawAttributes([
            'name' => $data_user['name'],
            'email' => $data_user['email'],
            'password' => Hash::make($data_user['password']),
            'cpf' => Hash::make($data_user['cpf']),
            'birthday' => $data_user['birthday'],
        ]);
        if (!$user->save()) {
            throw new AuthenticationException(__('register.error'));
        }
        $user->setAttribute('token', $user->createToken('app-token')->plainTextToken);
        return $user;
    }

    /**
     * @param string $cpf
     * @return bool
     */
    public function existsCPF(string $cpf)
    {
        return $this->model->select('cpf')->get()->filter(function (User $user) use ($cpf) {
                $user_cpf = Crypt::decryptString($user->cpf);
                return getNumbers($user_cpf) == getNumbers($cpf);
            })->count() > 0;
    }

    /**
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function pagination(Request $request)
    {
        $limit = $request->has('limit')? $request->get('limit') : 15;
        return $this->model->with('runnings')->paginate($limit);
    }

    /**
     * @param string $user_id
     * @param string $running_id
     * @return Model
     */
    public function registrationRunning(string $user_id, string $running_id)
    {

        /**
         * @var User $User
         * @var Running $Running
         * @var ServiceRunning $ServiceRunning
         *
         * User | Running Not Found Exception
         */
        $ServiceRunning = ServiceRunning::make();
        $User = $this->findOrFail($user_id);
        $Running = $ServiceRunning->getRunningOpenedById($running_id);
        if($ServiceRunning->existsUserInRunningByDate($user_id, $Running->start_at)){
            $msg = "Atleta já cadastrado. Data e Registro: {$Running->start_at->format('d/m/Y H:i')} | {$Running->id}";
            throw new \RuntimeException($msg);
        }

        return $Running->users()->save($User);
    }


}
