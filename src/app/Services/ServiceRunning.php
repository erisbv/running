<?php


namespace App\Services;


use App\Models\Running;
use App\Models\User;
use http\Exception\InvalidArgumentException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use RuntimeException;
use Throwable;

/**
 * Class ServiceRunning
 * @package App\Services
 *
 * @property-read array $_rules
 * @property-read Model|Running|Collection|Running[]|Builder $model
 *
 */
class ServiceRunning extends AbsServices
{
    /**
     * @var array
     */
    private array $ranges = [
        '18-24' => 24,
        '25-34' => 34,
        '35-44' => 44,
        '45-54' => 54,
        '55+' => 55,
    ];

    /**
     * ServiceRunning constructor.
     * @param Running $running
     */
    public function __construct(Running $running)
    {
        $this->model = $running;
    }

    /**
     * @param string $running_id
     * @return mixed
     */
    public function getRunning(string $running_id)
    {
        /**
         * @var Running $running
         */
        $running = $this->findOrFail($running_id);
        $running->load('users');
        return $running;
    }

    /**
     * @param string $running_id
     * @return Builder|Model|object|null|Running
     * @throws RuntimeException
     */
    public function getRunningOpenedById(string $running_id)
    {
        $Running = $this->model
            ->with('users')
            ->where('id', $running_id)
            ->whereNull('finished_at')
            ->first();
        if (!$Running) {
            throw new RuntimeException("Essa corrida não é válida ou já encerrou", Response::HTTP_BAD_REQUEST);
        }
        return $Running;
    }

    /**
     * @param array $data_running
     * @return Running|Running[]|Model|Collection
     * @throws Throwable
     */
    public function store(array $data_running)
    {
        $this->model->setAttribute('name', $data_running['name']);
        $this->model->setAttribute('start_at', $data_running['start_at']);
        $this->model->setAttribute('run_type_id', $data_running['run_type_id']);
        $this->model->saveOrFail();

        return $this->model;
    }

    /**
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function pagination(Request $request)
    {
        $limit = $request->has('limit') ? $request->get('limit') : 15;
        return $this->model->with('users')->paginate($limit);
    }

    /**
     * @param string $user_id
     * @param Carbon $date
     * @return bool
     */
    public function existsUserInRunningByDate(string $user_id, Carbon $date): bool
    {
        return $this->model
                ->where(DB::raw('DATE(start_at)'), '=', $date->format('Y-m-d'))
                ->withCount(['users' => function ($q) use ($user_id) {
                    return $q->where('id', $user_id);
                }])
                ->get()
                ->filter(function (Running $running) {
                    return $running->users_count > 0;
                })
                ->count() > 0;
    }

    /**
     * Ends run of athlete
     *
     * @param string $user_id
     * @param string $running_id
     * @param string $date_time
     */
    public function finishedRunning(string $user_id, string $running_id, string $date_time): void
    {
        try {
            $time = Carbon::createFromFormat('Y-m-d H:i:s.u', $date_time);
            if ($time === false) {
                throw new InvalidArgumentException("Data inválida", Response::HTTP_BAD_REQUEST);
            }
            /**
             * @var Running $Running
             */
            $Running = $this->findOrFail($running_id);
            if (!empty($Running->finished_at)) {
                throw new RuntimeException("Corrida já encerrada", Response::HTTP_BAD_REQUEST);
            }
            $time = Carbon::parse(now()->addMinute(rand(15, 60)))->toDateTimeString('milliseconds');
            $Running->users()->updateExistingPivot($user_id, ['running_x_users.finished_at' => $time]);
            $Running->refresh()->load('users');
        } catch (Throwable $throwable) {
            throw new RuntimeException(
                "Erro ao finalizar corredor: " . $throwable->getMessage(),
                Response::HTTP_BAD_REQUEST
            );
        }
    }

    /**
     * @param Running $running
     * @return false|int
     */
    public function existsPendingFinish(Running $running)
    {
        $total = $running->users()->whereNull('running_x_users.finished_at')->count();
        return (0 === $total) ? false : $total;
    }

    /**
     * @param Running $running
     * @return false|int
     */
    public function existsPendingRanking(Running $running)
    {
        $total = $running->users()->where('running_x_users.ranking', '0')->count();
        return (0 === $total) ? false : $total;
    }

    /**
     * @param Running $running
     * @return void
     * @throws Throwable
     */
    public function setRanking(Running $running): void
    {
        /**
         * @var User[] $running_users
         */
        $running_users = $running->users()->orderBy('running_x_users.finished_at', 'DESC')->get();
        $total = $running_users->count();
        // SET TIME FINISH
        $running->finished_at = $running_users[0]->pivot->finished_at;
        $running->saveOrFail();
        // SET RANKING
        $running_users->each(function (User $user, $key) use ($total, $running) {
            $raking = $total - $key;
            $running->users()->updateExistingPivot($user->id, ['ranking' => $raking], false);
        });
    }

    /**
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function listRunningByBirthday()
    {
        /**
         * @var Collection $data_running
         */
        $data_running = [];
        $this->model
            ->whereNotNull('runnings.finished_at')
            ->with(['runType', 'users' => function ($q) {
                $q->orderBy('running_x_users.ranking', 'ASC');
            }])
            ->orderBy('runnings.start_at', 'DESC')
            ->get()
            ->map(function (Running $running, $index) use (&$data_running) {
                $data_running[$index] = [
                    'running' => $running->getAttributes(),
                    'run_type' => $running->runType->getAttributes()
                ];

                $running->users->each(function (User $user) use (&$data_running, $index) {
                    $age = Carbon::parse($user->birthday)->age;
                    foreach ($this->ranges as $key => $breakpoint) {
                        if ($breakpoint >= $age) {
                            $data_running[$index]['users'][$key][] = $user;
                            break;
                        }
                    }
                });
            });
        return $data_running;
    }

    /**
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function listRunningRanking()
    {
        return $this->model
            ->whereNotNull('runnings.finished_at')
            ->with(['runType', 'users' => function ($q) {
                $q->orderBy('running_x_users.ranking', 'ASC');
            }])
            ->orderBy('runnings.start_at', 'DESC')
            ->get();
    }
}
