<?php


namespace App\Exceptions;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Foundation\Http\Exceptions\MaintenanceModeException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Session\TokenMismatchException;
use Illuminate\Support\Arr;
use Illuminate\Validation\ValidationException;
use InvalidArgumentException;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;
use HttpException;

class Handler extends ExceptionHandler
{
    private static array $_exception;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param Throwable $exception
     * @throws Throwable
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Throwable $exception
     * @return JsonResponse|Response
     * @throws Throwable
     *
     */
    public function render($request, Throwable $exception)
    {
        self::$_exception = [
            'type' => class_basename($exception),
            'Message' => $exception->getMessage(),
            'Code' => $exception->getCode(),
            'File' => $exception->getFile(),
            'Line' => $exception->getLine(),
            'Previous' => $exception->getPrevious(),
            'Trace' => $exception->getTrace(),
        ];

        if ($exception instanceof QueryException) {
            return self::errorResponse('Conflito ou erro de sintaxe no banco.', Response::HTTP_CONFLICT);
        }

        if ($exception instanceof NotFoundHttpException) {
            return self::errorResponse('Rota inexistente.', Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof ModelNotFoundException) {
            $modelName = class_basename($exception->getModel());
            $msg = "Instância ou modelo especificado ({$modelName}) não existe.";
            return self::errorResponse($msg, Response::HTTP_NOT_FOUND);
        }

        if ($exception instanceof InvalidArgumentException) {
            return self::errorResponse($exception->getMessage(), Response::HTTP_BAD_REQUEST);
        }

        if ($exception instanceof ValidationException) {
            return self::errorResponse($exception, Response::HTTP_BAD_REQUEST, Arr::except($request->input(), $this->dontFlash));
        }

        if ($exception instanceof AuthenticationException) {
            return self::errorResponse($exception->getMessage(), Response::HTTP_UNAUTHORIZED, Arr::except($request->input(), $this->dontFlash));
        }

        if ($exception instanceof AuthorizationException) {
            return self::errorResponse($exception->getMessage(), Response::HTTP_FORBIDDEN);
        }

        if ($exception instanceof HttpException) {
            return self::errorResponse($exception->getMessage(), Response::HTTP_EXPECTATION_FAILED);
        }

        if ($exception instanceof HttpResponseException) {
            return self::errorResponse($exception->getMessage(), Response::HTTP_BAD_REQUEST);
        }

        if ($exception instanceof TokenMismatchException) {
            return self::errorResponse('Token inválido.', Response::HTTP_UNAUTHORIZED);
        }

        if ($exception instanceof ThrottleRequestsException) {
            return self::errorResponse($exception->getMessage(), Response::HTTP_UNAUTHORIZED);
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            return self::errorResponse('Url existente, porém o método é inválido ou foram enviados parâmetros incorretos.', Response::HTTP_METHOD_NOT_ALLOWED);
        }

        if ($exception instanceof MaintenanceModeException) {
            return self::errorResponse('Sistema em manutenção.', Response::HTTP_UNAUTHORIZED);
        }

        if ($exception instanceof RuntimeException) {
            return self::errorResponse($exception->getMessage(), Response::HTTP_REQUEST_TIMEOUT);
        }

        if (config('app.debug')) {
            return parent::render($request, $exception);
        }

        $default_msg = 'Erro inesperado, tente novamente. Caso persista, entre em contato com mepague@mepague.net.';
        return self::errorResponse($default_msg, 500);
    }

    public static function errorResponse($message, $code, $inputs = null)
    {
        $debug = null;
        if (env('APP_DEBUG', false)) {
            $debug = self::$_exception ?? null;
        }
        return response()->json(
            [
                'type' => $debug['type'],
                'message' => $message,
                'code' => $code,
                'inputs' => $inputs,
                'debug' => $debug,
            ],
            $code
        );
    }
}
