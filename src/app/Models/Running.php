<?php

namespace App\Models;

use App\Models\Traits\Uuid;
use Carbon\CarbonInterface;
use Carbon\CarbonInterval;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;

/**
 * Class Running
 *
 * @package App\Models
 * @property int $running_type_id
 * @property string $name
 * @property string|date|Carbon $start_at
 * @property string|date|Carbon $finished_at
 * @property string $id
 * @property int $run_type_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\RunType $runType
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|Running newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Running newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Running query()
 * @method static \Illuminate\Database\Eloquent\Builder|Running whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Running whereFinishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Running whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Running whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Running whereRunTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Running whereStartAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Running whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Running extends Model
{
    use HasFactory, Uuid;

    public $incrementing = false;
    public $keyType = 'string';
    protected $dateFormat = 'Y-m-d H:i:s.u';

    protected $fillable = [
        'running_type_id',
        'name',
        'start_at',
        'finished_at',
    ];

    protected $casts = [
        'start_at' => 'datetime', // 2021-06-05T13:00:00
        'finished_at' => 'datetime', // 2021-06-05T13:00:00
    ];

    /**
     * @return string
     */
    public function getDateFormat()
    {
        return $this->dateFormat;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getTotalTimeAttribute()
    {
        $start_at = Carbon::parse($this->start_at)->format('Y-m-d H:i:s.u');
        $finished_at = Carbon::parse($this->finished_at)->format('Y-m-d H:i:s.u');
        $diff = CarbonInterval::make( Carbon::parse($start_at)->diff($finished_at) );
        return $diff->forHumans(null, true) . " " . $diff->milliseconds . "ms";
    }

    /**
     * @return int
     */
    public function getTotalUsersAttribute()
    {
        return collect($this->users)->count();
    }

    /**
     * @return BelongsTo|RunType
     */
    public function runType()
    {
        return $this->belongsTo(RunType::class);
    }

    /**
     * @return BelongsToMany|User[]
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'running_x_users')
            ->withTimestamps()
            ->withPivot('finished_at', 'ranking');
    }
}
