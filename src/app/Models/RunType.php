<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RunningType
 *
 * @package App\Models
 * @property string $distance
 * @property string $Type
 * @property int $id
 * @property string $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Running[] $Runnings
 * @property-read int|null $runnings_count
 * @method static \Illuminate\Database\Eloquent\Builder|RunType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RunType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|RunType query()
 * @method static \Illuminate\Database\Eloquent\Builder|RunType whereDistance($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RunType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|RunType whereType($value)
 * @mixin \Eloquent
 */
class RunType extends Model
{
    use HasFactory;
    public $timestamps = false;

    protected $fillable = [
        'distance',
        'type',
    ];

    public function getRunTypeNameAttribute()
    {
        return "{$this->distance}{$this->type}";
    }

    public function runnings()
    {
        return $this->hasMany(Running::class);
    }

}
