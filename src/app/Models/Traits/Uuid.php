<?php

namespace App\Models\Traits;

use Illuminate\Support\Str;

/**
 * Trait Uuid
 * @package App\Models\Traits
 *
 * @property string $primaryKey
 * @property string $keyType
 * @property bool $incrementing
 *
 */
trait Uuid
{
    /**
     * method bootCheckId
     *
     * Boot Uuid
     * @return void
     */
    public static function bootUuid(): void
    {
        self::creating(function ($model) {
            if (!isset($model->id)) {
                $model->id = (string)Str::uuid();
            }
        });
    }
}
