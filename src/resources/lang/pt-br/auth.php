<?php

return [
    'failed' => 'Essas credenciais não correspondem aos nossos registros',
    'password' => 'A senha fornecida está incorreta.',
    'throttle' => 'Muitas tentativas. Tente novamente em :seconds segundos.',
];
