<?php
return [
    'success' => 'Usuário registrado com sucesso',
    'error' => 'Erro desconhecido ao tentar registrar usuário.'
];
