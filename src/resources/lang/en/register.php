<?php
return [
    'success' => 'User successfully registered.',
    'error' => 'Unknown error when trying to register user.'
];
