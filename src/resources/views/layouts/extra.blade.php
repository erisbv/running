<section class="mt-5 mb-5">
    <div class="container-alt container">
        <div class="row">
            <div class="col-12 text-center">
                <div class="home-wrapper m-t-40">
                    <div class="mb-4">
                        <img src="{{ URL::asset('assets/images/logo-branco.png') }}" alt="logo" height="200" />
                    </div>

                    @yield('container-extra')
            </div>
        </div>
    </div>
</section>
