<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=1, maximum-scale=1.0, minimal-ui">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <meta name="url" content="{{url('/')}}"/>
    <meta name="uri" content="{{getRequestRoute()->uri}}"/>
    <meta name="author" content="{{config('app.name')}}"/>
    <title>
        @if (trim($__env->yieldContent('title')))
            @yield('title') | {{config('app.name')}}
        @else
            {{config('app.name')}}
        @endif
    </title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com"/>
    <!-- Favicon and touch icons  -->
    <link rel="shortcut icon" href="{{ asset('assets/images/icon-16.png') }}"/>
    <link rel="shortcut icon" href="{{ asset('assets/images/icon-32.png') }}"/>
    <link rel="shortcut icon" href="{{ asset('assets/images/icon-48.png') }}"/>
    <link rel="shortcut icon" href="{{ asset('assets/images/icon-64.png') }}"/>
    <link rel="shortcut icon" href="{{ asset('assets/images/icon-114.png') }}"/>
    <link rel="shortcut icon" href="{{ asset('assets/images/icon-128.png') }}"/>
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.ico') }}"/>
    <link rel="apple-touch-icon-precomposed" href="{{ asset('assets/images/icon-158.png') }}"/>

    <!-- App css -->
    <link href="{{ asset('assets/css/app.css?') . time() }}" rel="stylesheet" type="text/css"/>
    @stack('css')
</head>
<body data-cs="true">
<!-- Begin page -->
<div id="app">
    @yield('content')
</div>

<!-- App's Basic Js  -->
<script src="{{ asset('assets/js/plugins.js') . '?' . time() }}"></script>
<script src="{{ asset('assets/js/app.js') . '?' . time() }}"></script>
<script src="{{ asset('assets/js/app.jquery.js') . '?' . time() }}"></script>

<!-- script -->
@stack('script')

</body>
</html>
