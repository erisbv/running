/* ========================================================================
 * DataGrid - Eris Batista
 * ========================================================================
 *  Forma de Usar - Uso dinâmico
 * ======================================================================== */
(function ($) {
    'use strict';
    let $formCobranca = jQuery('.formCobranca');

    $.fn.appFormCobranca = function () {
        /*
         * private $root
         */
        let $root = this;

        // SHOW HIDE BTN
        $root.$btnShowHide = $('.btnInputShowHide');
        // SUBMIT BTN
        $root.$btnSubmit = $('.btnAddAss');
        // STATUS FORM
        $root.$status = $('#statusForm');

        // INPUTS
        $root.$inputValor = $('#valor');
        $root.$inputIntervalo = $('#intervalo');
        $root.$spanValor = $('.valor-dinamico.valor');
        $root.$spanIntervalo = $('.valor-dinamico.intervalo');

        // SELECT ASS
        $root.$selectAssinatura = $('#selectAssinatura');

        // URK PRINCIPAL
        $root.url = $root.prop('action');

        // FLAGS
        $root.modalClose = true;

        let _alertaStatus = {
            show: function (status, texto, load = false) {
                let loadText = (load) ? '<i class="fa fa-refresh fa-spin"></i>&nbsp;&nbsp;' : '<i class="fa fa-info"></i>&nbsp;&nbsp;';
                $root.$status.html('<div class="alert alert-' + status + '"><h4 class="p-0 m-0">' + loadText + texto + '</h4></div>');
            },
            hide: function () {
                $root.$status.html('');
            }
        };

        let _setInputAssinatura = function (assinaturaId) {
            if (assinaturaId) {
                $root.$btnSubmit.removeClass('disabled');

                $root.$inputValor.val('');
                $root.$inputIntervalo.val('');
                $root.$spanValor.html('');
                $root.$spanIntervalo.html('');
            } else {
                $root.$btnSubmit.addClass('disabled');

                $root.$inputValor.val('');
                $root.$inputIntervalo.val('');
                $root.$spanValor.html('');
                $root.$spanIntervalo.html('');
            }

        };

        let _selectAssinatura = function () {
            $root.$selectAssinatura.change(function () {
                let $this = $(this);
                let cobrancaAssinaturaId = parseInt($this.val());

                _alertaStatus.show('info', 'Carregando dados da Assinatura', true);

                _getAssinaturaPeloId(cobrancaAssinaturaId);

                _setInputAssinatura();

            });
        };

        let _getAssinaturaPeloId = function (cobrancaAssinaturaId) {
            let Assinatura = _ajaxRequest('', {'id': cobrancaAssinaturaId});
        };

        let _bindSubmit = function () {

            $root.$btnSubmit.addClass('disabled');

            $root.$btnSubmit.on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();
                let $this = $(this);

                if ($this.hasClass('disabled')) {
                    return false;
                }

                $root.modalClose = !$this.hasClass('repeat');

                // _$btnSubmit.addClass('disabled');

            });
        };

        let _bindShowHide = function () {
            $root.$btnShowHide.on('click', function (e) {
                e.preventDefault();
                e.stopPropagation();

                let $this = $(this);
                let dataShow = $this.data('show');
                let dataHide = $this.data('hide');

                let $inputShow = $('#' + dataShow);
                let $inputHide = $('#' + dataHide);

                $('[data-show="' + dataShow + '"]').removeClass('btn-default').addClass('btn-success');
                $('[data-show="' + dataHide + '"]').removeClass('btn-success').addClass('btn-default');

                $inputHide.val('').addClass('hide');
                $inputShow.val('').removeClass('hide');

            });
        };

        /*
         * Private Request
         */
        let _ajaxRequest = function (_url, _dataPost, _method = 'POST') {
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').prop('content')
                },
                /*
                beforeSend: function () {
                    $root.html($root.ajaxLoadHtml);
                },
                */
                cache: false,
                dataType: 'json',
                method: _method,
                data: _dataPost,
                url: _url.toString().trim(),
            }).done(function (result) {
                $root.handleData(result);
            }).fail(function (result) {
                window.alert('Falha ao executar solicitação: Cadastro/Edição de Cobrança');
                $root.handleData(false);
                return false;
            });
        };

        this.handleData = function (result) {
            if (!result) {
                $root.$status.html('<div class="alert alert-danger">Erro no processamento!</div>');
                throw new $root.dataException('Erro ao tentar carregar informações de paginação.', 'Processo não realizado');
            }
            $root.$status.html('<div class="alert alert-success">Processamento finalizado com sucesso!</div>');
        };

        $(window).ready(function () {
            _bindSubmit();
            _bindShowHide();
            _selectAssinatura();
            // _ajaxRequest();
        });

        return this;
    };

    jQuery(window).ready(function () {
        $formCobranca.each(function () {
            jQuery(this).appFormCobranca();
        });
    });

})(jQuery);

/*
let _ajaxRequest = function (url_pagination) {
            // URL  GET PAGINATION
            let url = _url.toString().trim();
            if (url_pagination) {
                url = (url_pagination).toString().trim();
            }
            let dataPost = {
                'view': _view
            };
            if(typeof _params !== 'undefined') {
                dataPost.params = _params;
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').prop('content')
                },
                beforeSend: function () {
                    $root.html($root.ajaxLoadHtml);
                },
                cache: false,
                dataType: $root.dataType,
                method: 'POST',
                data: dataPost,
                url: url
            }).done(function (result) {
                $root.handleData(result);
            }).fail(function (result) {
                if (option.debug) {
                    console.log('=== Falha no Processo ===');
                    console.log(result);
                }
                window.alert('Falha ao executar solicitação: ' + _handleName);
                $root.handleData(false);
                return false;
            }).always(function () {
                _root_height = $root.find('table').height();
                $root.height(_root_height);
            });
        };

        this.ajaxLoadHtml = '<div class="ajax-loading-data-grid"><p><i class="fa fa-refresh fa-spin"></i> Carregando</p></div>';

        this.dataException = function (message, dataName) {
            this.message = message;
            this.name = "Parâmetro DATA: " + dataName;
        };

        this.handleData = function (result) {
            if (!result) {
                $root.html('<div class="alert alert-danger">Erro ao tentar carregar informações de paginação.</div>');
                throw new $root.dataException('Erro ao tentar carregar informações de paginação.', 'Processo não realizado');
            }
            $root.html(result);
        };

        this.debug = function () {
            console.log('url: ', $root.data('url'));
            console.log('name: ', $root.data('name'));
            console.log('view: ', $root.data('view'));
            console.log('params: ', $root.data('params'));
        };

        if (option.debug) {
            this.debug();
        }

        // ORDER BY
        $(document).on('click', option.seletorColuna, function (e) {
            e.preventDefault();
            e.stopPropagation();
            console.log($(this));
            //$root.column = $(this);
            //_ajaxRequest();
        });
 */
