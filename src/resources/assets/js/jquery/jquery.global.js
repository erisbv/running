let GlobalApp = {};

!function ($) {
    'use strict';
    $.browser = {};
    $.browser.msie = false;
    $.browser.version = 0;
    if (navigator.userAgent.match(/MSIE ([0-9]+)\./)) {
        $.browser.msie = true;
        $.browser.version = RegExp.$1;
    }

    let $body = $('body');
    let $html_body = $('html, body');

    const CONST_MASTER_PAGE = $('body > div:first'); //$('#app');
    const CONST_URL = $('meta[name="url"]').prop('content');
    const CONST_URI = $('meta[name="uri"]').prop('content');
    const CONST_CHECK_SESSION = $body.data('cs');
    const CONST_TOKEN = $('[name="csrf-token"]').attr('content');
    const COLORS = {
        dark: '#2a3142',
        danger: '#ec4561',
        warning: '#f8b425',
        info: '#38a4f8',
        success: '#02a499',
        primary: '#626ed4'
    };

    GlobalApp.url = CONST_URL;
    GlobalApp.uri = CONST_URI;
    GlobalApp.$wrapper = CONST_MASTER_PAGE;
    GlobalApp.token = CONST_TOKEN;
    GlobalApp.colors = COLORS;

    let $wizardContainer = $('.form-wizard');
    let $errorContainer = $('#global-container-errors');
    let $errorList = $('.list-errors', $errorContainer);

    GlobalApp.exception = (message, dataName) => {
        this.message = message;
        this.name = (dataName) ? "Parâmetro DATA: " + dataName : 'Exception GlobalApp';
    };
    GlobalApp.getUrl = url => {
        return GlobalApp.url + '/' + url;
    };
    // GlobalApp.highlight = () => { return $.fn.effect( "highlight", {color:"#669966"}, 3000 ) };

    /**
     * ===========================================
     *  TOOLTIP
     * ===========================================
     */
    GlobalApp.toolTip = () => {
        $(document.body).find('[data-toggle="tooltip"]').tooltip();
        $(document.body).find('[data-toggle="popover"]').popover({trigger: 'focus'});
    };

    /**
     * ===========================================
     *  AJAX
     * ===========================================
     */
    GlobalApp.ajaxRequest = (url, options) => {
        let opt = {
            handleCall: false,
            handleName: false,
            dataType: 'json',
            method: 'GET',
            data: false,
            alerts: true
        };
        let formRequest = {};
        $.extend(formRequest, opt, options);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': GlobalApp.token
            },
            beforeSend: function () {
                // jsGlobal.ajaxLoadingAnimate.show();
            },
            cache: false,
            dataType: formRequest.dataType,
            method: formRequest.method,
            data: formRequest.data,
            url: url
        }).done(function (result) {
            if (formRequest.handleCall !== false) {
                formRequest.handleCall(result);
            }
        }).fail(function (result) {
            if (formRequest.alerts) {
                GlobalApp.alert.warning(
                    'Erro na Requisição.\nFalha ao executar solicitação.\nCaso o erro persista, entre em contato com o suporte. ',
                    formRequest.handleName?? 'Error'
                );
            }
            if (formRequest.handleCall !== false) {
                formRequest.handleCall(result);
            }
            return false;
        }).always(function () {
            setTimeout(function () {
                // jsGlobal.ajaxLoadingAnimate.hide();
                // jsGlobal.read();
            }, 2000);
        });
    };
    /*
        Ex.:
        GlobalApp.ajaxRequest(url, function (result) {
                if (!result) {
                    GlobalApp.alert.warning('Erro ao tentar carregar informações.', 'Busca de Cliente');
                    return false;
                }
            },
            'Busca de Clientes'
        );
    */
    GlobalApp.ajaxLoadingAnimate = {
        show: function () {
            // SET LOADING
            jsGlobal.$AjaxGridLoading.removeClass('hide');
            jsGlobal.$AjaxGridLoading.width(jsGlobal.$Window.width());
            jsGlobal.$AjaxGridLoading.height(jsGlobal.$Window.height());

            jsGlobal.$AjaxGridLoading.find('p').css({
                right: '50px'
            });
        },
        hide: function () {
            jsGlobal.$AjaxGridLoading.find('p').css({
                right: '-250px'
            });
            setTimeout(function () {
                jsGlobal.$AjaxGridLoading.height(0);
            }, 200);
        }
    };

    /**
     * ===========================================
     *  MODALS
     * ===========================================
     */
    GlobalApp.simpleModal = () => {
        $('.simple-modal').on('show.bs.modal', function (event) {
            let myTitle = $(event.relatedTarget).data('title');
            let myContent = $(event.relatedTarget).data('content');

            $(this).find(".modal-title").text(myTitle);
            $(this).find(".modal-body").text(myContent);
        });
    };

    GlobalApp.zIndexModal = () => {
        $(document).on('show.bs.modal', '.modal', function () {
            let zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function () {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);

            if(!$html_body.hasClass('removeScrolling')){
                $html_body.addClass('removeScrolling');
            }
        });
        $(document).on('hidden.bs.modal', '.modal', function () {
            if ($('.modal:visible').length === 0) {
                $html_body.removeClass('removeScrolling');
            }
        });
    };

    /**
     * ===========================================
     *  SWEET ALERT
     * ===========================================
     */
    GlobalApp.alert = {
        basic: (text, title, type) => {
            if (!title) {
                Swal.fire(text);
                return;
            }
            if (!type) {
                type = 'question';
            }
            // Swal.fire(title, text, type);
            Swal.fire({
                title: (title) ? title : 'Atenção',
                html: text,
                type: type,
                showCancelButton: false
            });
        },
        question: (text, title, handleCall) => {
            Swal.fire({
                title: title,
                html: text,
                type: 'question',
                showCancelButton: true,
                confirmButtonText: 'Confirmar',
                cancelButtonColor: '#ec4561'
            }).then((result) => {
                if (handleCall) {
                    handleCall(result);
                }
            });
        },
        success: (text, title, confirmButtonColor, showCancelButtonColor) => {
            confirmButtonColor = (confirmButtonColor) ? confirmButtonColor : '#626ed4';
            Swal.fire(
                {
                    title: (title) ? title : 'Operação realizada com sucesso',
                    html: text,
                    type: 'success',
                    showCancelButton: !!(showCancelButtonColor),
                    confirmButtonColor: confirmButtonColor,
                    cancelButtonColor: '#ec4561'
                }
            );
        },
        warning: (text, title, cancelButton, callBack) => {
            if (!callBack) {
                return GlobalApp.alert.basic(text, title, 'warning');
            }
            Swal.fire({
                title: (title) ? title : 'Atenção',
                html: text,
                type: "warning",
                showCancelButton: cancelButton,
                confirmButtonColor: COLORS.primary,
                cancelButtonColor: COLORS.danger
            }).then(function (result) {
                if(!callBack) {
                    if (result.value) {
                        Swal.fire("Ação executada!", "A solicitação foi executada.", "success");
                    }
                    return;
                }
                callBack(result);
            });
        },
        bootstrap: (text, title, showCancelButton = true) => {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger mr-2'
                },
                buttonsStyling: false,
            });
            swalWithBootstrapButtons.fire({
                title: title,
                html: text,
                type: 'warning',
                showCancelButton: (showCancelButton),
                reverseButtons: true
            }).then((result) => {
                if (result.value) {
                    swalWithBootstrapButtons.fire('Ação Executada', 'A solicitação foi executada.', 'success');
                } else if (
                    // Read more about handling dismissals
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire('Cancelado!', 'A solicitação não foi executada.', 'error');
                }
            });
        }
    };

    /**
     * ===========================================
     *  FORMS
     * ===========================================
     */
    // LABEL REQUIRED
    GlobalApp.formRequiredLabel = () => {
        let $inputs_required = $(document).find('input,textarea,select');
        $inputs_required.filter("[class~='required'], [required]").each(function () {
            let $this = $(this);
            let $label = $this.closest('div.form-group').find('label');
            if(!$label.find('span.required').length){
                $label.append(' (<span class="required text-danger">*</span>)');
            }
        });
    };
    // DATA-AUTOCOMPLETE
    GlobalApp.formAutoComplete = () => {
        let $form_autocomplete = $('form[data-autocomplete]');
        $form_autocomplete.each(function () {
            let $this = $(this);
            if($this.data('autocomplete')) {
                $this.find('text').prop('autocomplete', 'off');
            }
        });
    };

    // VALIDATION FORM OPTIONS
    GlobalApp.validationSettings = {
        //debug: true,
        ignore: '',
        lang: 'pt-BR',
        // errorElement: '<div>',
        // errorClass: '',
        errorContainer: $errorContainer,
        errorLabelContainer: $errorList,
        // wrapper: 'li',
        // onsubmit: false,
        errorPlacement: function (error, element) {
            error.addClass("help-block");
        },
        unhighlight: function (element, errorClass, validClass) {
            //$(element).closest('.form-group').addClass("has-success").removeClass("has-error");
        },
        highlight: function (element) {
            //$(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        },
        success: function (element) {
            //$(element).closest('.form-group').removeClass('has-error');
        },
        invalidHandler: function (event, validator) {
            let errors = validator.numberOfInvalids();
            if (errors) {
                let message = errors === 1
                    ? 'Existe apenas um campo com erro no formulário, verfique em destaque.'
                    : 'Existem ' + errors + ' erros. Verfique a lista de erros.';
                GlobalApp.alert.warning(message, 'Erro no formulário');
            } else {
                GlobalApp.validationSettings.errorContainer.hide();
            }
        }
    };
    // VALIDATION FORM
    GlobalApp.isValidForm = ($form) => {
        let $formValidate = $form.validate();
        if (!$form.valid()) {
            $formValidate.focusInvalid();
            return false;
        }
        return true;
    };
    //FORM MASK MONEY
    GlobalApp.formMaskMoney = ($e) => {
        if ($().maskMoney()) {
            let $maskMoney = ($e) ? $e.find('[data-mask-money]') : $('body').find('[data-mask-money]');
            let prefix = ($maskMoney.data('mask-money-prefix'))?? '';
            let suffix = ($maskMoney.data('mask-money-suffix'))?? '';
            $maskMoney.maskMoney({
                prefix: prefix,
                suffix: suffix,
                thousands: '.',
                decimal: ',',
            });
        }
    };
    // FORM MASK
    GlobalApp.formMask = () => {
        // CPF CNPJ
        let $cpf_cnpj = $(document).find('[name*="cpf_cnpj"]');
        let $_cpf = $(document).find('[name="cpf"]');
        let $_cnpj = $(document).find('[name="cnpj"]');
        // NAO E PADRAO DO SISTEMA
        if ($_cpf.length) {
            $_cpf.data('mask', '999.999.999-99');
        }
        // NAO E PADRAO DO SISTEMA
        if ($_cnpj.length) {
            $_cnpj.data('mask', '99.999.999/9999-99');
        }
        // PADRAO DO SISTEMA - ATIVA O RECURSO DE COMPLEMENTO
        if ($cpf_cnpj.length) {
            let oldValor = '';
            $cpf_cnpj.on('keydown, keyup', function () {
                let $this = $(this);
                let originalValor = $this.val();

                // GlobalApp.registerTypePerson(originalValor);

                setTimeout(function () {
                    let v = $this.val().replace(/\D/g, "");
                    // MAX DIGITS CNPJ 33333333333333 / 33333333333
                    if (v.length > 14) {
                        $this.val(oldValor);
                        return false;
                    }
                    if (v.length === 14) {
                        $this.val(originalValor);
                        oldValor = originalValor;
                        return false;
                    }
                    if (v.length <= 11) { //CPF
                        v = v.replace(/(\d{3})(\d)/, "$1.$2");
                        v = v.replace(/(\d{3})(\d)/, "$1.$2");
                        v = v.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
                    } else { //CNPJ
                        v = v.replace(/^(\d{2})(\d)/, "$1.$2");
                        v = v.replace(/^(\d{2})\.(\d{3})(\d)/, "$1.$2.$3");
                        v = v.replace(/\.(\d{3})(\d)/, ".$1/$2");
                        v = v.replace(/(\d{4})(\d)/, "$1-$2");
                    }
                    $this.val(v);
                }, 1);
            });
        }

        if ($().inputmask()) {
            let $dataMask = $('[data-mask]');
            if ($dataMask.length) {
                $dataMask.each(function () {
                    let $thisInputMask = $(this);
                    // console.log('$thisInputMask', $thisInputMask.prop('id'));
                    //https://jsfiddle.net/jLepcmsx/10/
                    if ($thisInputMask.data('maskOptions')) {
                        let maskType = $thisInputMask.data('mask');
                        let maskOptions = $thisInputMask.data('maskOptions');
                        if (maskType === 'percent') {
                            maskType = 'decimal';

                            maskOptions.integerDigits = 2;
                            maskOptions.digits = 2;
                            maskOptions.digitsOptional = false;
                            maskOptions.numericInput = true;
                        }
                        $thisInputMask.inputmask(maskType, maskOptions);
                    } else {
                        $thisInputMask.inputmask($thisInputMask.data('mask'));
                    }
                });
            }
        }
    };
    /*
    GlobalApp.btnDelete = (route) => {
    <form method="POST" action="{{--{{route('panel.settings.table-prices.products.destroy', $tp_product->id)}}--}}" accept-charset="UTF-8"
        style="display:inline">
            {{ method_field('DELETE') }}
        {{ csrf_field() }}
    <button type="submit" class="btn btn-danger btn-xs" title="Deletar" onclick="return confirm('Confirma a exclusão?')">
            <i class="far fa-trash-alt"></i>
            </button>
            </form>
    };
    */

    GlobalApp.registerTypePerson = (cpf_cnpj) => {
        let $typePerson = $('[name="type"]');
        let $company = $('[name="company_name"], [name="fantasy_name"], [name="cnae"]');

        if(cpf_cnpj.length < 11) {
            $typePerson.val('');
            $company.prop('disabled', 'disabled');
        }
        if (cpf_cnpj.length < 15) {
            $typePerson.val('PF');
            $company.prop('disabled', 'disabled');
        } else {
            $typePerson.val('PJ');
            $company.removeAttr('disabled');
        }
    };
    // FORMAT CURRENCY
    GlobalApp.formatMoney = (number, prefix, decPlaces, decSep, thouSep) => {
        decPlaces = isNaN(decPlaces = Math.abs(decPlaces)) ? 2 : decPlaces;
        decSep = typeof decSep === "undefined" ? "," : decSep;
        thouSep = typeof thouSep === "undefined" ? "." : thouSep;
        prefix = typeof prefix !== "undefined"  ? prefix : '';

        let sign = number < 0 ? "-" + prefix : prefix;
        let i = String(parseInt(number = Math.abs(Number(number) || 0).toFixed(decPlaces)));
        let j = (i.length) > 3 ? i % 3 : 0;

        return sign +
            (j ? i.substr(0, j) + thouSep : "") +
            i.substr(j).replace(/(\decSep{3})(?=\decSep)/g, "$1" + thouSep) +
            (decPlaces ? decSep + Math.abs(number - i).toFixed(decPlaces).slice(2) : "");
    };
    // FORM WIZARD
    GlobalApp.formWizard = {
        // SET ACTIVE FORM
        setActiveNavItem: (index, style) => {
            let $navLink = $('a[data-toggle="tab"]', $wizardContainer);
            let $navItems = $('.nav-item', $wizardContainer);
            if (typeof index === 'undefined') {
                $navItems.removeClass('active');
                $navLink.on('shown.bs.tab', e => {
                    let $activatedTab = $(e.target);
                    index = parseInt($activatedTab.data('index'));
                    for (let i = 0; i <= index; i++) {
                        $navItems.eq(i).addClass('active');
                    }
                });
                return;
            }
            $navItems.eq(index).addClass(style);
        },
        navLinks: () => {
            if ($wizardContainer.length) {
                let $navLinks = $('.nav-link', $wizardContainer);
                let $form = $wizardContainer.closest('form');
                if (parseInt($form.length) === 0) {
                    GlobalApp.alert.warning("Formulário Wizard iniciado sem o Obejto Form local.");
                    return false;
                }

                $navLinks.on('click', function (e) {
                    e.preventDefault();
                    GlobalApp.formWizard.setActiveNavItem();
                });
            }// end if
        },
        init: () => {
            GlobalApp.formWizard.navLinks();
        }
    };

    /*
     * ===========================================
     *  LINKS - PREVENT DEFAULT
     * ===========================================
     */
    GlobalApp.logout = () => {
        let $linkLogout = $('a.btn-logout');
        $linkLogout.on('click', function (e) {
            e.preventDefault();
            e.stopPropagation();

            $(this).addClass('disabled');

            GlobalApp.ajaxRequest(CONST_URL + '/logout', {
                handleCall: function (data_return) {
                    window.location.href = CONST_URL + '/login';
                },
                handleName: 'Desconectar',
                dataType: 'json',
                method: 'POST',
                data: {
                    '_token': CONST_TOKEN
                },
                alerts: false
            });
        });
    };

    /*
     * ===========================================
     *  DATEPICKER
     * ===========================================
     */
    GlobalApp.datePicker = () => {
        let $datepicker = $('.datepicker');
        $datepicker.datepicker({
            language: 'pt-BR',
            autoclose: true,
            startDate: '+2d'
        });
    };

    /*
     * ===========================================
     *  SELECT 2
     * ===========================================
     */
    GlobalApp.select2 = () => {
        let $_s2 = jQuery(document.body).find('.select2'); // SELECTOR SELECT2
        // console.log($_s2.length, $_s2.prop('name'), $_s2.prop('id'));
        if ($_s2.length) {
            $_s2.each(function () {
                let $_this = jQuery(this);
                let url = '';
                let selectOptions = {
                    language: "pt-BR",
                    dropdownParent: CONST_MASTER_PAGE,
                    tags: false
                };

                if (typeof $_this.data('placeholder') !== 'undefined') {
                    selectOptions.placeholder = $_this.data('placeholder');
                } else {
                    selectOptions.placeholder = '- Selecione uma opção -';
                }
                if (typeof $_this.data('parent') !== 'undefined') {
                    selectOptions.dropdownParent = $($_this.data('parent'));
                }
                if (typeof $_this.data('url') !== 'undefined') {
                    url = $_this.data('url');
                }
                if (typeof $_this.data('tags') !== 'undefined' || typeof $_this.prop('multiple') !== 'undefined') {
                    selectOptions.tags = true;
                }

                if (url.length) {
                    // LIST AJAX
                    selectOptions.templateSelection = function (data, container) {
                        return data.display;
                    };
                    selectOptions.createTag = function (params) {
                        return 'undefined';
                    };
                    selectOptions.minimumInputLength = 2;
                    selectOptions.minimumResultsForSearch = -1;
                    selectOptions.ajax = {
                        url: url,
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').prop('content')
                        },
                        dataType: 'json',
                        type: 'POST',
                        data: function (params) {
                            return {
                                q: $.trim(params.term)
                            };
                        },
                        processResults: function (data) {
                            return {
                                results: data
                            };
                        },
                        cache: true
                    };
                }
                $_this.select2(selectOptions);

                // SELECT2 IND NAVIGATION TAB
                // $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                //     GlobalApp.select2();
                // });

            });
        }
    };

    /**
     * ===========================================
     *  CHECK LOGIN
     * ===========================================
     */
    GlobalApp.checkSession = () => {
        if(!CONST_CHECK_SESSION) {
            return false;
        }

        $(window).on("blur focus", function (e) {
            let url = GlobalApp.getUrl('check-session');
            GlobalApp.ajaxRequest(url, {
                    alerts: false,
                    data: false,
                    handleCall: (result) => {
                        if(!result.user_session){
                            window.location.href = GlobalApp.getUrl('login');
                        }
                    }// handle call
                }// end options
            );
        });
    };

    /*
     * ===========================================
     *  BIND METHODS
     * ===========================================
     */
    GlobalApp.bind = () => {
        // GlobalApp.checkSession();
        GlobalApp.logout();
        GlobalApp.toolTip();

        // FORM
        GlobalApp.formRequiredLabel();
        GlobalApp.formAutoComplete();
        GlobalApp.formMask();
        GlobalApp.formMaskMoney();
        GlobalApp.formWizard.init();
        GlobalApp.select2();
        GlobalApp.datePicker();

        // MODALS
        GlobalApp.simpleModal();
        GlobalApp.zIndexModal();
    };

    $(document).ready(function () {
        "use strict";
        GlobalApp.bind();
    });

}(window.jQuery);
