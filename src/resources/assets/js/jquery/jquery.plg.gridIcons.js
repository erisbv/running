!function ($) {
    'use strict';
    /*
     * ===========================================
     *  ICON BTN GRID
     * ===========================================
     */
    $.fn.gridIcons = function (options) {
        let $root = this;
        let opt = {
            routes: {},
            id: null
        };
        opt = $.extend({}, opt, options);

        let $div_action_container = $('<div/>', {class: 'text-center'});
        let $containerBtns = $('<div/>', {class: 'btn-group btn-group-sm'});
        $.each(opt.routes, (key, val) => {
            let url = val.replace('_id_', opt.id);
            switch(key) {
                case 'edit':
                    $containerBtns.append('<a href="'+url+'" class="btn btn-dark btn-xs" data-toggle="tooltip" data-placement="top" title="Editar" data-original-title="Editar"><i class="mdi mdi-account-edit"></i></a>');
                    break;
                case 'show':
                    $containerBtns.append('<a href="'+url+'" class="btn btn-dark btn-xs" data-toggle="tooltip" data-placement="top" title="Visualizar" data-original-title="Visualizar"><i class="mdi mdi-account-search"></i></a>');
                    break;
                case 'destroy':
                    $containerBtns.append('<a href="'+url+'" class="btn btn-danger btn-xs" data-toggle="tooltip" data-placement="top" title="Deletar" data-original-title="Deletar" data-btn-delete><i class="mdi mdi-trash-can"></i></a>');
                    break;
                default:
                    // METODO PRECISA SER ESTUDADO APRA GERAR UM PADRAO GLOBAL FORA DO ESCOPO LARAVEL
                    $containerBtns.append('<a href="'+url+'" class="btn btn-dark btn-xs" data-toggle="tooltip" data-placement="top" title="Default Anchor" data-original-title="Default Anchor"><i class="ion ion-ios-link"></i></a>');
                    break;
            }
        });
        $div_action_container.append($containerBtns);

        return $div_action_container.html();
    };

}(window.jQuery);
