jQuery(function ($) {
    "use strict";

    let MainApp = function () {
        this.$body = $("body");
        this.$btnFullScreen = $("#btn-fullscreen");
        this.$leftMenuButton = $('.button-menu-mobile');
        this.$menuItem = $('.has_sub > a');
    };

    MainApp.prototype.intSlimscrollmenu = function () {
        $('.slimscroll-menu').slimscroll({
            height: 'auto',
            position: 'right',
            size: "10px",
            color: '#9ea5ab',
            wheelStep: 5,
            touchScrollStep: 50
        });
    },
        MainApp.prototype.initSlimscroll = function () {
            $('.slimscroll').slimscroll({
                height: 'auto',
                position: 'right',
                size: "10px",
                color: '#9ea5ab',
                touchScrollStep: 50
            });
        },

        MainApp.prototype.initMetisMenu = function () {
            //metis menu
            $("#side-menu").metisMenu({
                // bootstrap 4
                triggerElement: '.nav-link'
            });
        },

        MainApp.prototype.initLeftMenuCollapse = function () {
            // Left menu collapse
            $('.button-menu-mobile').on('click', function (event) {
                event.preventDefault();
                $("body").toggleClass("enlarged");
            });
        },

        MainApp.prototype.initEnlarge = function () {
            if ($(window).width() < 1025) {
                $('body').addClass('enlarged');
            } else {
                $('body').removeClass('enlarged');
            }
        },

        MainApp.prototype.initActiveMenu = function () {
            let get_url = GlobalApp.getUrl(GlobalApp.uri);
            let split_url = GlobalApp.uri.split(/[?#/]/);
            let active = false;
            let $MENU = $("#sidebar-menu a");

            // FILTER MENU BY URL SPLIT
            let filterActiveMenu = () => {
                let base_url = GlobalApp.url;
                split_url.forEach(function (url_param) {
                    base_url += '/' + url_param;
                    if (base_url.trim() === get_url.trim()) {
                        activeMenu($('[href="' + base_url + '"]'));
                        return;
                    }
                })
            };

            // ACTIVE MENU BY ELEMENT
            let activeMenu = ($e) => {
                active = true;
                $e.addClass("mm-active");
                $e.parent().addClass("mm-active"); // add active to li of the current link
                $e.parent().parent().addClass("mm-show");
                $e.parent().parent().prev().addClass("mm-active"); // add active class to an anchor
                $e.parent().parent().parent().addClass("mm-active");
                $e.parent().parent().parent().parent().addClass("mm-show"); // add active to li of the current link
                $e.parent().parent().parent().parent().parent().addClass("mm-active");
            };

            // FOREACH MENU
            $MENU.each(function () {
                if (active) {
                    return false;
                }
                if ($(this).prop('href').trim() === get_url.trim()) {
                    activeMenu($(this));
                }
            });

            if (!active) {
                filterActiveMenu();
            }
        },

        //full screen
        MainApp.prototype.initFullScreen = function () {
            let $this = this;
            $this.$btnFullScreen.on("click", function (e) {
                e.preventDefault();

                if (!document.fullscreenElement && /* alternative standard method */ !document.mozFullScreenElement && !document.webkitFullscreenElement) {  // current working methods
                    if (document.documentElement.requestFullscreen) {
                        document.documentElement.requestFullscreen();
                    } else if (document.documentElement.mozRequestFullScreen) {
                        document.documentElement.mozRequestFullScreen();
                    } else if (document.documentElement.webkitRequestFullscreen) {
                        document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                    }
                } else {
                    if (document.cancelFullScreen) {
                        document.cancelFullScreen();
                    } else if (document.mozCancelFullScreen) {
                        document.mozCancelFullScreen();
                    } else if (document.webkitCancelFullScreen) {
                        document.webkitCancelFullScreen();
                    }
                }
            });
        },


        MainApp.prototype.init = function () {
            this.intSlimscrollmenu();
            this.initSlimscroll();
            this.initMetisMenu();
            this.initLeftMenuCollapse();
            this.initEnlarge();
            this.initActiveMenu();
            this.initFullScreen();
            Waves.init();
        },

        //init
        $.MainApp = new MainApp;
    $.MainApp.Constructor = MainApp
});

//initializing
jQuery(function ($) {
    "use strict";
    $.MainApp.init();
});
