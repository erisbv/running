!function ($) {
    'use strict';
    /*
     * ===========================================
     *  ICON BTN
     * ===========================================
     */
    $.fn.btnIcon = function (options) {
        let $root = this;
        let opt = {
            iconStd: 'fa fa-user-plus',
            iconLoad: 'fa fa-spinner fa-spin',
            text: {
                std: '',
                load: 'Processando...',
            }
        };
        opt = $.extend({}, opt, options);
        $root.default = e => {
            if (!opt.text.std.length) {
                opt.text.std = $root.text();
            }
            let iconStd = '<span class="icon-block"><i class="' + opt.iconStd + '"></i></span>' + opt.text.std;
            $root.html(iconStd).removeAttr('disabled');
        };
        $root.loading = e => {
            let iconLoad = '<span class="icon-block"><i class="' + opt.iconLoad + '"></i></span>' + opt.text.load;
            $root.html(iconLoad).prop('disabled', 'disabled');
        };
        $root.enable = e => {
            $root.removeAttr('disabled');
        };
        $root.disable = e => {
            $root.prop('disabled', 'disabled');
        };
        return $root;
    };
}(window.jQuery);
