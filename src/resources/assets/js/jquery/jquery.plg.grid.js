!function ($) {
    /*
     * ===========================================
     *  DATA TABLE - GRID
     * ===========================================
     * EXEMPLO DE USO
     * -------------------------------------------------------------------------
     * <table data-tables>
     *
     <script type="text/javascript">
     $(function () {
        let options = {
            route: '{{route('panel.customers.index')}}',
            columns: [
                {data: 'id', visible: false, orderable: false}, // searchable: false, orderable: false, visible: false},
                {data: 'name'},
                {data: 'cpf_cnpj', orderable: false},
                {data: 'type', orderable: false},
                {data: 'email', orderable: false}, //, orderable: false},
     {data: 'phone', orderable: false}, //, orderable: false},
     ],
     btnRoutes: {
                show: "{{route('panel.customers.show', '_id_')}}",
                destroy: "{{route('panel.customers.destroy')}}"
            },
     btnRoutesColumn: 6
     };
     $.fn.grid(options);
     });
     </script>
     */
    'use strict';
    $.fn.grid = function (options) {
        let $root = this;
        let opt = {
            route: false,
            columns: false,
            gridContainer: false,
            btnRoutes: false,
            btnRoutesColumn: false,
            dom: 'Bfrtip',
            fixedColumns: true,
            lengthChange: false,
            processing: true,
            serverSide: true,
            buttons: [
                {extend: 'copyHtml5', text: 'Copiar'},
                {extend: 'excelHtml5', text: 'Excel'},
                {extend: 'csvHtml5', text: 'CSV'},
                {extend: 'pdfHtml5', text: 'PDF'},
                {extend: 'pageLength', text: "Exibindo 15 registros"}
            ],
            lengthMenu: [
                [15, 25, 50, -1],
                ['15 registros', '25 registros', '50 registros', 'Visualizar Todos']
            ],
            columnDefs: []
        };
        opt = $.extend({}, opt, options);

        $root.gridContainer = (opt.gridContainer === false)? $('[data-tables]') : opt.gridContainer;
        $root.addBtnActions = (columnTarget, btnRoutes) => {
            let btns = {
                targets: columnTarget,
                data: null,
                searchable: false,
                orderable: false,
                render: function (data, type, row, meta) {
                    // return $.fn.gridIcons({routes: $root.grid.data('btn-routes'), id: data.id});
                    let actions_btn_icon = $.fn.gridIcons({routes: btnRoutes, id: data.id});
                    let $div_action_container = $('<div/>', {class: 'text-center'});
                    $div_action_container.append(actions_btn_icon);
                    return $div_action_container.html();
                }
            };
            opt.columnDefs.push(btns);
        };

        if(!opt.route) {
            GlobalApp.alert.warning('Erro na Requisição.\nFalha ao executar grid.\nRota não informada.', 'Data Grid - Route Error');
            return false;
        }
        if(!opt.columns) {
            GlobalApp.alert.warning('Erro na Requisição.\nFalha ao executar grid.\nColunas não informadas.', 'Data Grid - Column Error');
            return false;
        }

        if(opt.btnRoutes && opt.btnRoutesColumn) {
            $root.addBtnActions(opt.btnRoutesColumn, opt.btnRoutes);
        }

        $root.generate = () => {
            $root.gridContainer.DataTable({
                dom: opt.dom,
                fixedColumns: opt.fixedColumns,
                lengthChange: opt.lengthChange,
                processing: opt.processing,
                serverSide: opt.serverSide,
                buttons: opt.buttons,
                lengthMenu: opt.lengthMenu,
                columns: opt.columns,
                columnDefs: opt.columnDefs,
                language: {
                    sEmptyTable: "Nenhum registro encontrado",
                    sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                    sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
                    sInfoFiltered: "(Filtrados de _MAX_ registros)",
                    sInfoPostFix: "",
                    sInfoThousands: ".",
                    sLengthMenu: "_MENU_ resultados por página",
                    sLoadingRecords: "Carregando...",
                    sProcessing: "",
                    sZeroRecords: "Nenhum registro encontrado",
                    sSearch: "Pesquisar",
                    oPaginate: {
                        sNext: "Próximo",
                        sPrevious: "Anterior",
                        sFirst: "Primeiro",
                        sLast: "Último"
                    },
                    oAria: {
                        sSortAscending: ": Ordenar colunas de forma ascendente",
                        sSortDescending: ": Ordenar colunas de forma descendente"
                    },
                    buttons: {
                        copyTitle: 'Cópia efetuada',
                        copySuccess: {
                            _: '%d linhas copiadas',
                            1: '1 linha copiada'
                        },
                        pageLength: {
                            '_': "Exibir %d registros",
                            '-1': "Exibir Todos"
                        }
                    }
                },
                ajax: {
                    url: opt.route
                }
            });
        };

        // $root.loading = e => {
        //     let iconLoad = '<span class="icon-block"><i class="' + opt.iconLoad + '"></i></span>' + opt.text.load;
        //     $root.html(iconLoad).prop('disabled', 'disabled');
        // };

        return $root.generate();
    };
}(window.jQuery);
