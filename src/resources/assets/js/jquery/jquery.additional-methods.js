(function() {
    jQuery.validator.addMethod("is_currency", function(value, element) {
        let valCurrency = parseFloat(value.replace(".", "").replace(",", "."));
        return typeof valCurrency === "number";
    }, 'O valor não é válido.');

    jQuery.validator.addMethod("min_value", function(value, element) {
        let minVal = 10;
        let val = value.replace('R$', '').trim();
        let valCurrency = parseFloat(val.replace(".", "").replace(",", "."));
        let rr = valCurrency > minVal;
        if(!rr) {
            console.error('ERRO NA COMRPAÇÃO DO VALOR', valCurrency, val);
        }
        return rr;
    }, 'O valor mínimo de cobrança é R$10,00.');
}());
