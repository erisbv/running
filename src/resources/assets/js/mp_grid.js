/* ========================================================================
 * DataGrid - Eris Batista
 * ========================================================================
 *  Forma de Usar - Uso dinâmico
 * ======================================================================== */
(function ($) {
    'use strict';
    $.fn.appGrid = function (options) {
        /*
         * private $root
         */
        let $root = this;
        let _root_height;
        // NAME CALL ACTION
        let _handleName = $root.data('name');
        // VIEW RENDER
        let _view = $root.data('view');
        // URK PRINCIPAL
        let _url = $root.data('url');
        // PARAMETROS
        let _params = $root.data('params');

        if (typeof _handleName === 'undefined') {
            throw new $root.dataException('Necessário o nome da Operação para logs do sistema', 'data-name');
        }
        if (typeof _view === 'undefined') {
            throw new $root.dataException('View de carregamento ajax não informada!!', 'data-view');
        }
        if (typeof _url === 'undefined') {
            throw new $root.dataException('url de carregamento ajax não informada!!', 'data-url');
        }
        let option = {
            seletorColuna: '[data-order]',
            linkPagination: '.pagination .page-item > a',
            dataType: 'json',
            debug: false
        };

        $.extend(option, options);

        let _ajaxBindPagination = function () {
            $(document).on('click', option.linkPagination, function (e) {
                e.preventDefault();
                e.stopPropagation();
                let $this = $(this);
                let url_pagination = $this.prop('href');
                _ajaxRequest(url_pagination);
            });
        };

        /*
         * Private Request
         */
        let _ajaxRequest = function (url_pagination) {
            // URL  GET PAGINATION
            let url = _url.toString().trim();
            if (url_pagination) {
                url = (url_pagination).toString().trim();
            }
            let dataPost = {
                'view': _view
            };
            if(typeof _params !== 'undefined') {
                dataPost.params = _params;
            }
            $.ajax({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').prop('content')
                },
                beforeSend: function () {
                    $root.html($root.ajaxLoadHtml);
                },
                cache: false,
                dataType: $root.dataType,
                method: 'POST',
                data: dataPost,
                url: url
            }).done(function (result) {
                $root.handleData(result);
            }).fail(function (result) {
                if (option.debug) {
                    console.log('=== Falha no Processo ===');
                    console.log(result);
                }
                window.alert('Falha ao executar solicitação: ' + _handleName);
                $root.handleData(false);
                return false;
            }).always(function () {
                _root_height = $root.find('table').height();
                //$root.height(_root_height);
                jQuery(document).find('[data-toggle="tooltip"]').tooltip();
            });
        };

        this.ajaxLoadHtml = '<div class="ajax-loading-data-grid"><p><i class="fa fa-refresh fa-spin"></i> Carregando</p></div>';

        this.dataException = function (message, dataName) {
            this.message = message;
            this.name = "Parâmetro DATA: " + dataName;
        };

        this.handleData = function (result) {
            if (!result) {
                $root.html('<div class="alert alert-danger">Erro ao tentar carregar informações de paginação.</div>');
                throw new $root.dataException('Erro ao tentar carregar informações de paginação.', 'Processo não realizado');
            }
            $root.html(result);
        };

        this.debug = function () {
            console.log('url: ', $root.data('url'));
            console.log('name: ', $root.data('name'));
            console.log('view: ', $root.data('view'));
            console.log('params: ', $root.data('params'));
        };

        if (option.debug) {
            this.debug();
        }

        /* ---------------------------------
        ORDER BY
        --------------------------------- */
        $(document).on('click', option.seletorColuna, function (e) {
            e.preventDefault();
            e.stopPropagation();
            console.log($(this));
            //$root.column = $(this);
            //_ajaxRequest();
        });

        /* ---------------------------------
        BTN OPEN GRID
        --------------------------------- */
        $(document).on('click', '[data-open-form]', function (e) {
            e.preventDefault();
            let IdEl = jQuery(this).data('openForm');
            if (jQuery(IdEl).hasClass('hide')) {
                jQuery(IdEl).removeClass('hide');
                return;
            }
            jQuery(IdEl).addClass('hide');
        });

        $(window).ready(function () {
            _ajaxBindPagination();
            _ajaxRequest();
        });

        return this;
    };

    /*
        jQuery(window).ready(function () {
            (new AppGrid()).bindClick();
        });
    */

})(jQuery);
