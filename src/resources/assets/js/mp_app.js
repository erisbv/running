let jsGlobal;
jsGlobal = {
    $appContainer: jQuery('#app'),
    $Window: jQuery(window),
    $ViewportHeight: jQuery(window).height(),
    $BodyHeight: jQuery(document).height(),
    $AjaxGridLoading: jQuery('.ajax-loading'),
    $_btnActiveModal: [], // REMOVE ACTIVE DOS BOTÕES DE MODAL

    FormPageLoginRegister: function () {
        let $BODY = jQuery('body.form-page');
        if ($BODY.length) {
            let $FormPageContainer = jQuery('.form-page-container');
            let $Video = jQuery('video');

            $BODY.height(this.$BodyHeight);
            $FormPageContainer.height(this.$BodyHeight);

            let videoWidth = $Video.width(),
                windowWidth = this.$Window.width(),
                marginLeftAdjust = (windowWidth - videoWidth) / 2;

            $Video.css({
                'height': this.$BodyHeight,
                'marginLeft': marginLeftAdjust
            });
        }
    },
    /* ---------------------------------
     TOOL TIPE
    --------------------------------- */
    ToolTip: function () {
        jQuery(document).find('[data-toggle="tooltip"]').tooltip();
    },
    /* ---------------------------------
     DATE PICKER
    --------------------------------- */
    datePicker: function () {
        let $Datepicker = jQuery('[data-datepicker]');
        $Datepicker.datepicker({
            autoclose: true
        });
    },
    /* ---------------------------------
     AJAX RQUEST
    --------------------------------- */
    ajaxRequest: function (url, handleData, handleName, dataType, method, data) {
        if (typeof dataType !== 'undefined') {
            dataType = 'json';
        }
        if (typeof method !== 'undefined') {
            method = 'GET';
        }
        if (typeof data !== 'undefined') {
            data = null;
        }

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').prop('content')
            },
            beforeSend: function () {
                jsGlobal.ajaxLoadingAnimate.show();
            },
            cache: false,
            dataType: dataType,
            method: method,
            data: data,
            url: url
        }).done(function (result) {
            handleData(result);
        }).fail(function () {
            if (!handleName) {
                handleName = 'Requisição Indefinida';
            }
            window.alert('Falha ao executar solicitação: ' + handleName);
            return false;
        }).always(function () {
            setTimeout(function () {
                jsGlobal.ajaxLoadingAnimate.hide();
                // jsGlobal.read();
            }, 2000);
        });
    },
    ajaxLoadingAnimate: {
        show: function () {
            // SET LOADING
            jsGlobal.$AjaxGridLoading.removeClass('hide');
            jsGlobal.$AjaxGridLoading.width(jsGlobal.$Window.width());
            jsGlobal.$AjaxGridLoading.height(jsGlobal.$Window.height());

            jsGlobal.$AjaxGridLoading.find('p').css({
                right: '50px'
            });
        },
        hide: function () {
            jsGlobal.$AjaxGridLoading.find('p').css({
                right: '-250px'
            });
            setTimeout(function () {
                jsGlobal.$AjaxGridLoading.height(0);
            }, 200);
        }
    },
    /* ---------------------------------
     CEP
    --------------------------------- */
    getCep: function () {
        jQuery(document).on('click', '.cep-search', function () {
            // OBJETO BTN
            let $this = jQuery(this);
            let $cep = $this.parent('.input-group-addon').prev();
            let cep = $cep.val().replace(/\D/g, '');
            let $thisIcon = jQuery(this);
            let group_id = $cep.data('group');

            if (!cep || cep.length < 8) {
                window.alert('CEP com menos de 8 dígitos');
                return false;
            }

            //Expressao regular para validar o CEP.
            let validacep = /^[0-9]{8}$/;
            if (!validacep.test(cep)) {
                window.alert('CEP inválido!');
                return false;
            }

            $thisIcon.find('i').prop('class', 'fa fa-fw fa-refresh fa-spin');
            //https://postmon.com.br/
            let postmon_webservice = 'https://api.postmon.com.br/v1/cep/' + cep;

            $.getJSON(postmon_webservice, function (dados) {
                if (("erro" in dados)) {
                    window.alert("CEP não encontrado.");
                    return false;
                }

                let $bairro = jQuery('[group="' + group_id + '"][name="Endereco[bairro]"]');
                let $logradouro = jQuery('[group="' + group_id + '"][name="Endereco[logradouro]"]');

                if ($bairro.val().length === 0) {
                    $bairro.val(dados.bairro);
                }
                if ($logradouro.val().length === 0) {
                    $logradouro.val(dados.logradouro);
                }

                // jQuery('#cidade_id').val(dados.cidade_id);
                jsGlobal.setCidadesPeloEstado(dados.estado, dados.cidade, group_id);
            }).done(function () {
                setTimeout(function () {
                    $thisIcon.find('i').prop('class', 'fa fa-fw fa-search');
                }, 500);
            }).fail(function () {
                window.alert("erro no processamento.");
            });

        });//end click
    },
    /* ---------------------------------
     SELECT CIDADES
    --------------------------------- */
    setCidadesPeloEstado: function (uf, cidade_nome, group_id) {
        let url = jQuery('body').data('url') + '/api/cidades-uf/' + uf;
        let $selectCidade = jQuery('[group="' + group_id + '"][name="Endereco[cidade_id]"]');
        $selectCidade.html('');
        $selectCidade.prop('placeholder', 'Selecione uma Cidade do Estado ' + uf);

        this.ajaxRequest(url, function (result) {
                if (!result) {
                    window.alert('Erro ao tentar carregar informações de relacionamento do Estado e Cidade.');
                    return false;
                }
                jQuery.each(result, function (index, cidade) {
                    $selectCidade.append('<option value="' + cidade.id + '">' + uf + ' - ' + cidade.nome + '</option>');
                });
                // console.log(uf+' - '+cidade_nome);
                $('[name="Endereco[cidade_id]"] option:contains(' + uf + ' - ' + cidade_nome + ')').attr('selected', 'selected').trigger('change');
            },
            'Busca de Cidades por UF: ' + uf
        );
    },
    /* ---------------------------------
     REMOVE CARAACTER ESPECIAL
    --------------------------------- */
    escapeSpecialChars(jsonString) {
        return jsonString.replace(/\n/g, "\\n").replace(/\r/g, "\\r").replace(/\t/g, "\\t").replace(/\f/g, "\\f");
    },
    /* ---------------------------------
    REQUIRED LABEL
    --------------------------------- */
    RequiredLabel: function () {
        let $inputs_required = jQuery('input,textarea,select').filter('[required]');
        $inputs_required.each(function () {
            let $this = jQuery(this);
            $this.closest('div.form-group').find('label').append(' (<span class="required text-danger">*</span>)');
        });
        // Remove bloco de alerta do INPUT
        jQuery('form:not(.form-show-error) .form-group').find('.help-block').hide();
    },
    /* ---------------------------------
    SELECT 2
    --------------------------------- */
    select2: function () {
        let $_s2 = jQuery('.select2');
        let $_s2DropdownParent = jQuery('#app');
        let placeholder = 'Selecione uma opção';

        if (jQuery().select2) {
            $_s2.each(function () {
                let $_this = jQuery(this);
                if(typeof $_this.data('placeholder') !== 'undefined') {
                    placeholder = $_this.data('placeholder');
                }
                if(typeof $_this.data('parent') !== 'undefined') {
                    $_s2DropdownParent = jQuery($_this.data('parent'));
                }

                $_this.select2({
                    language: "pt-BR",
                    dropdownParent: $_s2DropdownParent,
                    placeholder: placeholder
                });

            });
        }
    },
    selectMulta: function () {

        jQuery(document).on('change', 'select.select-multa-tipo', function () {
            // OBJETO SELECT
            let $this = jQuery(this);
            // ID ROW ATIVADO OU DESATIVADO
            let find_id = +$this.data('id');

            let $groupPercentual = jQuery('.multa-imput-container-porcentagem-' + find_id);
            let $groupFixa = jQuery('.multa-imput-container-fixa-' + find_id);

            if ($this.val().toString() === 'Fixa') {
                $groupPercentual.addClass('hide').find('.form-control').prop('disabled', true).val('');
                $groupFixa.removeClass('hide').find('.form-control').prop('disabled', false).val('');
            } else {
                $groupPercentual.removeClass('hide').find('.form-control').prop('disabled', false).val('');
                $groupFixa.addClass('hide').find('.form-control').prop('disabled', true).val('');
            }
        });

        jQuery(document).on('change', 'select.select-multa', function () {
            // OBJETO SELECT
            let $this = jQuery(this);
            // ID ROW ATIVADO OU DESATIVADO
            let find_id = +$this.data('id');
            // CONTAINER INPUTS
            let $containerJuros = jQuery('#multa-container-' + find_id);

            if (parseInt($this.val()) === 0) {
                $containerJuros.find('.form-control').prop('disabled', true);
                $containerJuros.find('input[type="text"]').val('');
            } else {
                $containerJuros.find('.form-control').prop('disabled', false);
            }
        });
    },

    bugsBootStrap: function () {

        // ATIVAR DROPDOWN
        jQuery(".dropdown-toggle").dropdown();

        /*
        // CRIA EVENTO DO BOTÃO DROPDOWN
        jQuery(document).on('click', '[data-toggle="modal"]', function (e) {
            e.preventDefault();
            //jQuery.inArray( "John", arr )
            let $this = jQuery(this);
            $_btnActiveModal
            if(typeof jsGlobal.$_btnActiveModal === 'undefined') {

            }
        });
        */

        // REMOVER PADDING RIGHT GERADO PELO MODAL
        jQuery('.modal').on('hidden.bs.modal', function (e) {
            jQuery('body').css('padding-right', '0px !important');
        });


    },

    /* ---------------------------------
     LOAD DA APLICAÇÃO
    --------------------------------- */
    read: function () {
        this.ToolTip();
    },
    init: function () {
        this.ToolTip();
        // LOGIN - REGISTER - RESEND
        this.FormPageLoginRegister();
        // FORMS
        this.RequiredLabel();
        this.select2();
        this.getCep();

        this.selectMulta();

        this.bugsBootStrap();
    }
};

jQuery(document).ready(function () {
    jsGlobal.init();
});

jQuery(window).resize(function () {
    jsGlobal.FormPageLoginRegister();
    jsGlobal.$Window = jQuery(window);
}).resize();

jQuery(window).ready(function () {
    jsGlobal.read();
});
