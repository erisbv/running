<?php

namespace Tests\Feature;

use App\Models\Running;
use App\Models\RunType;
use GuzzleHttp\Client;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ApiRunningTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    public function testApiRunningCreate()
    {
        /**
         * @var RunType $RunType
         * @var Running $Running
         */
        $RunType = RunType::factory()->createOne(['distance' => 21, 'type' => 'KM',]);
        $this->assertArrayHasKey('id', $RunType->toArray());
        $Running = Running::factory()->createOne([['run_type_id' => $RunType->id,]]);

        $this->assertArrayHasKey('run_type_id', $Running->toArray());
        $this->assertArrayHasKey('name', $Running->toArray());
        $this->assertArrayHasKey('start_at', $Running->toArray());

    }

}
