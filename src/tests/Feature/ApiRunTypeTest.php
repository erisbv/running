<?php

namespace Tests\Feature;

use App\Models\Running;
use App\Models\RunType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ApiRunTypeTest extends TestCase
{

    use RefreshDatabase, WithoutMiddleware;

    public function testApiRunTypeCreate()
    {
        /**
         * @var RunType $RunType
         */
        $RunType = RunType::factory()->createOne(['distance' => 21, 'type' => 'KM',]);
        $this->assertInstanceOf(RunType::class, $RunType, 'Run Type Register: Object User not instance of Model\RunType');
        $attributes = $RunType->getAttributes();
        $this->assertArrayHasKey('distance', $attributes, 'Run Type Properties: distance uninformed');
        $this->assertArrayHasKey('type', $attributes, 'Run Type Properties: type uninformed');
    }

}
