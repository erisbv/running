<?php

namespace Tests\Feature;

use App\Models\User;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tests\TestCase;

/**
 * Class ApiAuthenticationTest
 * @package Tests\Feature
 *
 */
class ApiUserActionsTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware;

    /***
     * @covers \App\Http\Controllers\Auth\RegisterController::store
     * @return void
     * @throws Exception
     */
    public function testApiUserInstance(): void
    {
        $User = User::factory()->createOne(
            $this->getDataUserFaker()
        );

        $this->assertInstanceOf('App\Models\User', $User, 'User Register: Object User not instance of Model\User');
    }

    /**
     * @throws Exception
     */
    public function testUserPropertiesForLogin(): void
    {
        $User = User::factory()->createOne(
            $this->getDataUserFaker()
        );

        $attributes = $User->getAttributes();
        $this->assertArrayHasKey('name', $attributes, 'Properties: name uninformed');
        $this->assertArrayHasKey('email', $attributes, 'Properties: email uninformed');
        $this->assertArrayHasKey('password', $attributes, 'Properties: password uninformed');
    }

    /**
     *
     */
    public function testApiAuthenticate(): void
    {
        $User = User::factory()->createOne(
            $this->getDataUserFaker()
        );

        $response = $this->post(route('auth.login'), [
            'email' => $User->email,
            'password' => self::USER_PASS,
        ]);

        $response->assertStatus(200);

        // Test Return
        $this->assertArrayHasKey('data', $response->original, 'Return Api Login: data::user uninformed');
        $this->assertTrue(is_array($response->original['data']));

        // Test Token
        $token = $response->original['data']['token'];
        $result_digit_token = preg_match('/^\d+|/', $token, $matches, PREG_UNMATCHED_AS_NULL);
        $this->assertTrue(false != $result_digit_token, 'Return Api Login: Sanctum Token error pattern (regex)');
        $this->assertTrue(strlen($token) == 42, 'Return Api Login: Sanctum Token error pattern (length)');

        $this->assertArrayHasKey('id', $response->original['data'], 'Return Api Login: user_id uninformed');
        $this->assertArrayHasKey('email', $response->original['data'], 'Return Api Login: e-mail uninformed');
        $this->assertArrayHasKey('token', $response->original['data'], 'Return Api Login: token uninformed');
    }

}
