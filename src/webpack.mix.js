const mix = require('laravel-mix');

mix.autoload({
    jquery: ['$', 'window.jQuery', "jQuery", "window.$", "jquery", "window.jquery-pages"],
    'popper.js/dist/umd/popper.js': ['Popper']
});

mix.options({
    processCssUrls: false,
    autoprefixer: false // autoprefixer: { remove: false }
});

mix.webpackConfig({
    module: {
        rules: [
            {
                test: /\.tsx?$/,
                loader: "ts-loader",
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: ["*", ".js", ".jsx", ".vue", ".ts", ".tsx"]
    }
});

/**
 * ====================================================================================================================
 * VUE JS
 * ====================================================================================================================
 */
mix.js('resources/js/app.js', 'public/assets/js/app.js').vue({
    extractStyles: true,
    globalStyles: false
});

/**
 * ====================================================================================================================
 * PLUGINS JS
 * ====================================================================================================================
 */
mix.scripts(
    [
        'resources/assets/js/jquery/jquery.min.js',
        'resources/plugins/jquery-ui/jquery-ui.min.js',
        'resources/assets/js/jquery/bootstrap.bundle.min.js',
        // JS MENU =======================================================
        'resources/assets/js/jquery/metis-menu.min.js',
        // SCROLL =======================================================
        'resources/assets/js/jquery/jquery.slimscroll.js',
        'resources/assets/js/jquery/waves.min.js',
        // ALERTS =======================================================
        'resources/plugins/sweet-alert2/sweetalert2.min.js',
        // VALIDATION =======================================================
        'resources/plugins/jquery-validation/jquery.validate.min.js',
        'resources/plugins/jquery-validation/localization/messages_pt_BR.js',
        // MASK =======================================================
        // 'resources/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js',
        'resources/plugins/jquery-inputmask/jquery.inputmask.js',
        'resources/plugins/jquery-mask-money/jquery.maskMoney.min.js',
        // DATE PICKER =======================================================
        'resources/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
        'resources/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.pt-BR.min.js',
        // SELECT 2 =======================================================
        'resources/plugins/select2/js/select2.min.js',
        'resources/plugins/select2/js/i18n/pt-BR.js',
        // 'resources/plugins/select2/js/select2.full.min.js',
        // DATATABLE =======================================================
        // Required datatable js
        'resources/plugins/datatables/jquery.dataTables.min.js',
        'resources/plugins/datatables/dataTables.bootstrap4.min.js',
        // Buttons
        'resources/plugins/datatables/dataTables.buttons.min.js',
        'resources/plugins/datatables/buttons.bootstrap4.min.js',
        'resources/plugins/datatables/jszip.min.js',
        'resources/plugins/datatables/pdfmake.min.js',
        'resources/plugins/datatables/vfs_fonts.js',
        'resources/plugins/datatables/buttons.html5.min.js',
        'resources/plugins/datatables/buttons.print.min.js',
        'resources/plugins/datatables/buttons.colVis.min.js',
        // Responsive
        'resources/plugins/datatables/dataTables.responsive.min.js',
        'resources/plugins/datatables/responsive.bootstrap4.min.js',
    ],
    'public/assets/js/plugins.js'
);

/**
 * ====================================================================================================================
 * MY SCRIPTS JQUERY
 * ====================================================================================================================
 */
mix.scripts(
    [
        // MY PLUGINS
        'resources/assets/js/jquery/jquery.plg.btnIcon.js',
        'resources/assets/js/jquery/jquery.plg.grid.js',
        'resources/assets/js/jquery/jquery.plg.gridIcons.js',
        'resources/assets/js/jquery/jquery.additional-methods.js',
        // js theme
        'resources/assets/js/jquery/jquery.theme.js',
        //js global
        'resources/assets/js/jquery/jquery.global.js'
    ],
    'public/assets/js/app.jquery.js'
);

/**
 * ====================================================================================================================
 * COPY PATHS
 * ====================================================================================================================
 */
mix.copyDirectory('resources/plugins', 'public/plugins');
mix.copyDirectory('resources/assets/icons/_fonts', 'public/assets/_fonts');
mix.copyDirectory('resources/assets/images', 'public/assets/images');
mix.copyDirectory('resources/assets/svg', 'public/assets/svg');

/**
 * ====================================================================================================================
 * CSS - BOOTSTRAP & FONTS + PLUGINS - THEME
 * ====================================================================================================================
 */
mix.sass('resources/sass/app.scss',    'public/assets/css/app.css');
