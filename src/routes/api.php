<?php

use Illuminate\Support\Facades\Route;

Route::middleware('auth:sanctum')
    ->group(function () {
        require __DIR__ . '/api_users.php';
        require __DIR__ . '/api_runnings.php';
        require __DIR__ . '/api_lists.php';
    });

require __DIR__.'/auth.php';
