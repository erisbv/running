<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\v1\RunningController;

Route::apiResource('/runnings', RunningController::class);
Route::post('/runnings/add-result', [RunningController::class, 'finishedRunning'])->name('user.finished-running');
Route::post('/runnings/add-result', [RunningController::class, 'finishedRunning'])->name('user.finished-running');
