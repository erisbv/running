<?php

use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Auth\ConfirmablePasswordController;
use App\Http\Controllers\Auth\EmailVerificationNotificationController;
use App\Http\Controllers\Auth\EmailVerificationPromptController;
use App\Http\Controllers\Auth\NewPasswordController;
use App\Http\Controllers\Auth\PasswordResetLinkController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\VerifyEmailController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\LoginController;

Route::name('auth.')
    //->middleware('throttle:3,1')
    ->group(function () {
        Route::post('/login', [\App\Http\Controllers\Auth\LoginController::class, 'store'])->name('login');
        Route::post('/logout', [\App\Http\Controllers\Auth\LoginController::class, 'destroy'])->name('logout');
        Route::post('/forgot-password', [PasswordResetLinkController::class, 'store'])->name('password.email');
        Route::post('/reset-password', [NewPasswordController::class, 'store'])->name('password.update');
        Route::post('/register', [RegisterController::class, 'store'])->name('register');
        Route::post('/confirm-password', [ConfirmablePasswordController::class, 'store'])->name('confirm-password');
        Route::post('/email/verification-notification', [EmailVerificationNotificationController::class, 'store'])->name('verification.send');
    });


//Route::get('/register', [RegisteredUserController::class, 'create'])->name('register');
//Route::get('/forgot-password', [PasswordResetLinkController::class, 'create'])->name('password.request');
//Route::get('/reset-password/{token}', [NewPasswordController::class, 'create'])->name('password.reset');
//Route::get('/verify-email', [EmailVerificationPromptController::class, '__invoke'])->middleware('auth')->name('verification.notice');
//Route::get('/verify-email/{id}/{hash}', [VerifyEmailController::class, '__invoke'])->middleware(['auth', 'signed', 'throttle:6,1'])->name('verification.verify');
//Route::get('/confirm-password', [ConfirmablePasswordController::class, 'show'])->middleware('auth')->name('password.confirm');
