<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\v1\UserController;

Route::apiResource('/users', UserController::class)->except('store');
Route::post('/users/add-running', [UserController::class, 'registrationRunning'])->name('user.registration-running');
