<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\{
    LoginController,
    ConfirmPasswordController,
    ForgotPasswordController,
    RegisterController
};

Route::get('/', [LoginController::class, 'showLoginForm'])->name('home');
Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
Route::get('/password/confirm', [ConfirmPasswordController::class, 'showConfirmForm'])->name('confirm-password');
Route::get('/password/reset', [ForgotPasswordController::class, 'showLinkRequestForm'])->name('reset-password');
Route::get('/password/reset/{token}', [ForgotPasswordController::class, 'showResetForm'])->name('reset-password-token');
//Route::get('/register', [RegisterController::class, 'register'])->name('register');

//require __DIR__ . '/web_panel.php';
//Auth::routes();
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//Route::resource('users', \App\Http\Controllers\UserController::class);
