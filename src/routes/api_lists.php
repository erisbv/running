<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\v1\RunningController;

Route::get('/list/sort-age', [RunningController::class, 'listRunningByBirthday'] )->name('list-sort-age');
Route::get('/list/sort-ranking', [RunningController::class, 'listRunningRanking'] )->name('list-sort-age');
