<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DashboardController;

Route::middleware('auth:sanctum')
    ->prefix('panel')
    ->name('panel')
    ->group(function () {
        Route::get('/dashboard', [DashboardController::class, 'index']);
    });
