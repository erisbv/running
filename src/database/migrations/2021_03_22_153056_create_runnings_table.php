<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRunningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('runnings', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->unsignedSmallInteger('run_type_id')->index();
            $table->string('name', 50)->unique();
            $table->dateTime('start_at', 3);
            $table->dateTime('finished_at', 3)->nullable();
            $table->timestamps();

            $table->foreign('run_type_id')
                ->references('id')
                ->on('run_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('runnings');
    }
}
