<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRunningUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('running_x_users', function (Blueprint $table) {
            $table->uuid('user_id');
            $table->uuid('running_id');
            $table->dateTime('finished_at', 3)->nullable();
            $table->smallInteger('ranking')->default(0);
            $table->timestamps();

            $table->primary(['user_id', 'running_id']);

            $table->foreign('user_id')->on('users')->references('id')->cascadeOnDelete();
            $table->foreign('running_id')->on('runnings')->references('id')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('running_x_users');
    }
}
