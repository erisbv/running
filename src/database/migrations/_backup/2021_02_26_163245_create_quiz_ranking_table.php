<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizRankingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_ranking', function (Blueprint $table) {
            $table->id();
            $table->uuid('user_id');
            $table->unsignedSmallInteger('total_hits')->comment('Users total question Hits');
            $table->unsignedSmallInteger('total_errors')->comment('Users total question Errors');
            $table->timestamps();
            $table->foreign('user_id', 'qr_users')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_ranking');
    }
}
