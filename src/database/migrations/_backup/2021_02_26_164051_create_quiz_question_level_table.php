<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizQuestionLevelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_question_level', function (Blueprint $table) {
            $table->uuid('user_id')->comment('User the question');
            $table->uuid('quiz_question_id')->comment('Question');
            $table->unsignedSmallInteger('quiz_level_id')->comment('Question Level');

            $table->primary(['user_id', 'quiz_question_id', 'quiz_level_id'], 'qql_');
        });

        Schema::table('quiz_question_level', function (Blueprint $table){
            $table->foreign('user_id', 'qql_users')->references('id')->on('users');
            $table->foreign('quiz_question_id', 'qql_questions')->references('id')->on('quiz_questions');
            $table->foreign('quiz_level_id', 'qql_levels')->references('id')->on('quiz_levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_question_level');
    }
}
