<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_replies', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->uuid('quiz_question_id');
            $table->string('reply')->comment('Question Reply');
            $table->boolean('is_image')->default(false);
            $table->timestamps();
            $table->foreign('quiz_question_id', 'qr_questions')->references('id')->on('quiz_questions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_replies');
    }
}
