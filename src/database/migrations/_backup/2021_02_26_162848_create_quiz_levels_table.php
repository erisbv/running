<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizLevelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_levels', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('name', 50)->comment('Name of Levels Quiz');
            $table->unsignedSmallInteger('factor_point')->comment('Factor point');
            $table->smallInteger('min')->comment('Min points the level');
            $table->string('icon')->nullable()->comment('Icon of Level');
            $table->smallInteger('order')->comment('Order the levels');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_levels');
    }
}
