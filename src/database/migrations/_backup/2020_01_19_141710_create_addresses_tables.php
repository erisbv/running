<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // STATE
        Schema::create('address_states', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->string('uf', 5);
            $table->string('name', 55);
        });

        // CITY
        Schema::create('address_cities', function (Blueprint $table) {
            $table->smallIncrements('id');
            $table->unsignedSmallInteger('address_state_id');
            $table->string('name', 155);
            $table->string('slug', 170);

            $table->foreign('address_state_id')->references('id')->on('address_states')->onDelete('cascade');
        });

        // ADDRESS
        Schema::create('addresses', function (Blueprint $table) {
            $table->bigIncrements('id')->comment("ENDERECOS DOS CONTATOS");
            $table->unsignedSmallInteger('address_city_id')->index();

            $table->string('zip_code',80)->comment("CEP");
            $table->string('district',200)->comment("BAIRRO");
            $table->string('street',200)->comment("RUA");
            $table->string('street_number',80)->comment("NÚMERO DA CASA");
            $table->string('country',100)->default('BRAZIL')->comment("PAIS");

            $table->boolean('is_primary')->default(false);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('address_city_id')->references('id')->on('address_cities');
        });

        // ADDRESS X USERS
        Schema::create('address_user', function (Blueprint $table) {
            $table->unsignedBigInteger('address_id');
            $table->uuid('user_id');
            $table->primary(['address_id', 'user_id']);
            // SOFT DELETE
            $table->softDeletes();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('address_id')->references('id')->on('addresses')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_states');
        Schema::dropIfExists('address_cities');
        Schema::dropIfExists('address_user');
        Schema::dropIfExists('addresses');
    }
}
