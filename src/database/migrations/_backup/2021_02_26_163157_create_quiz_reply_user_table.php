<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuizReplyUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quiz_reply_user', function (Blueprint $table) {
            $table->id();
            $table->uuid('user_id');
            $table->uuid('quiz_question_id');
            $table->uuid('quiz_reply_id');
            $table->timestamps();

            $table->foreign('user_id', 'qru_users')->references('id')->on('users');
            $table->foreign('quiz_question_id', 'qru_questions')->references('id')->on('quiz_questions');
            $table->foreign('quiz_reply_id', 'qru_reply')->references('id')->on('quiz_replies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quiz_reply_user');
    }
}
