<?php

namespace Database\Factories;

use App\Models\RunType;
use Illuminate\Database\Eloquent\Factories\Factory;

class RunTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RunType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            //
        ];
    }
}
