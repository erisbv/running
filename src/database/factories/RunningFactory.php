<?php

namespace Database\Factories;

use App\Models\Running;
use Illuminate\Database\Eloquent\Factories\Factory;

class RunningFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Running::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => "Running " . $this->faker->unique()->randomNumber(2),
            'start_at' => now()->format('Y-m-d H:i:s.u'),
        ];
    }
}
