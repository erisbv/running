<?php

namespace Database\Seeders;

use App\Models\Running;
use App\Models\RunType;
use App\Models\User;
use App\Services\ServiceRunning;
use Illuminate\Database\Seeder;
use Throwable;

/**
 * Class usersSeeder
 * @package Database\Seeders
 */
class RunningAndUsersSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     * @throws Throwable
     */
    public function run()
    {
        // GLOBAL USER|RUNNING|TYPE
        foreach ([3, 5, 10, 21] as $distance_km) {
            print str_pad(" Criando Corrida e Gerando Participantes {$distance_km}KM", 80, '-', STR_PAD_LEFT) . PHP_EOL;
            // CREATE TYPE RUNNER
            $RunType = RunType::factory()->createOne(['distance' => $distance_km, 'type' => 'KM',]);
            // CREATE RUNNER
            $Running = Running::factory()->createOne(['run_type_id' => $RunType->id,]);
            // CREATE USERS
            $this->createUsers($Running);
            // CHECK PENDING USERS
            $total_pending_finish = ServiceRunning::make()->existsPendingFinish($Running);
            if (!$total_pending_finish) {
                $this->generateRanking($Running);
            }
        } // end foreach
    }

    /**
     * @param Running $running
     */
    private function createUsers(Running $running): void
    {
        // users
        User::factory()
            ->count(30)
            ->create()
            ->each(function (User $user) use ($running) {
                // SAVE USER AND ADD RUNNER
                $running->users()->save($user);
                // FAKE TIME FINISH
                $time = now()->addMinute(rand(15, 60))->format('Y-m-d H:i:s.u');
                // FINISH RUNNING
                ServiceRunning::make()->finishedRunning($user->id, $running->id, $time);
            }); // end each User
    }

    /**
     * @param Running $running
     * @throws Throwable
     */
    private function generateRanking(Running $running): void
    {
        print str_pad(" GERANDO RANKING: {$running->name} ", 80, '.', STR_PAD_RIGHT) . PHP_EOL;
        $ServiceRunning = ServiceRunning::make();
        // CHECK ALL RANKED
        $total_pending_ranking = $ServiceRunning->existsPendingRanking($running);
        if (false !== $total_pending_ranking) {
            // GENERATE RANKING
            $ServiceRunning->setRanking($running);
        }
    }
}
